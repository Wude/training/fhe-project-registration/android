@echo off

setlocal

cd "%USERPROFILE%/AppData/Local/Android/Sdk"

for /f "tokens=* usebackq" %%f in (`emulator\emulator -list-avds`) do (set "IMAGE=%%f" & goto :endfor)
:endfor
echo %IMAGE%
emulator\emulator -avd %IMAGE%

endlocal
