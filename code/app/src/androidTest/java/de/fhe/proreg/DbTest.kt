package de.fhe.proreg

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.fhe.proreg.data.local.Db
import kotlinx.coroutines.runBlocking
import java.time.LocalDateTime
import java.util.UUID
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.get

// /**
//  * Instrumented test, which will execute on an Android device.
//  *
//  * See [testing documentation](http://d.android.com/tools/testing).
//  */
// @RunWith(AndroidJUnit4::class)
// class RoomDbTest: KoinTest {

//     val roomTestModule = module {
//         single {
//             Room.inMemoryDatabaseBuilder(get(), Db::class.java)
//                 .allowMainThreadQueries()
//                 .build()
//         }
//         single {
//             get<Db>().studentProjectAccess()
//         }
//         single {
//             get<Db>().studentProjectDocumentAccess()
//         }
//     }

//     @Before
//     fun setup() {
//         loadKoinModules(roomTestModule)
//     }

//     @After
//     fun tearDown() {
//         val db = get<DiaryDatabase>()
//         db.close()

//         unloadKoinModules(roomTestModule)
//     }

//     @Test
//     fun writeReadDeleteSingleStudentProjectAccess() = runBlocking {

//         val dao = get<StudentProjectAccess>()

//         assertTrue("DB should start empty", dao.getAll().isEmpty())

//         val entityID = insertStudentProjectEntity(dao)
//         assertTrue("DB should contain one entry", dao.getAll().size == 1)

//         val entity = studentProjectDocumentDao.get(entityID)
//         kotlin.test.assertNotNull(entity, "DB Entity should have been found")

//         dao.delete(entity)
//         assertNull("DB should not contain deleted Entity", dao.get(entityID))
//         assertTrue("DB should be empty after deletion", dao.getAll().isEmpty())
//     }

//     @Test
//     fun writeReadDeleteSingleStudentProjectDocumentAccess() = runBlocking {

//         val dao = get<StudentProjectDocumentAccess>()

//         assertTrue("DB should start empty", dao.getAll().isEmpty())

//         val entityID = insertStudentProjectEntity(dao)
//         assertTrue("DB should contain one entry", dao.getAll().size == 1)

//         val entity = dao.get(entityID)
//         kotlin.test.assertNotNull(entity, "DB Entity should have been found")

//         dao.delete(entity)
//         assertNull("DB should not contain deleted Entity", dao.get(entityID))
//         assertTrue("DB should be empty after deletion", dao.getAll().isEmpty())
//     }

//     private fun insertStudentProjectEntity(dao: StudentProjectAccess): UUID {
//         val entity = StudentProjectEntity(
//             projectID = UUID.randomUUID(),
//             title = "Testproject",
//         )
//         dao.insert(entity)
//         return entity.projectID
//     }

//     private fun insertStudentProjectDocumentEntity(dao: StudentProjectDocumentAccess): UUID {
//         val entity = StudentProjectDocumentEntity(
//             projectID = UUID.randomUUID(),
//             documentID = UUID.randomUUID(),
//         )
//         dao.insert(entity)
//         return entity.documentID
//     }
// }
