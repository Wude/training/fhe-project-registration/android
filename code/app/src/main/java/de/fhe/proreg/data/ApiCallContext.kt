package de.fhe.proreg.data

import de.fhe.proreg.data.network.model.BaseResponse
import com.squareup.moshi.Moshi
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.adapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeoutOrNull
import retrofit2.Response
import timber.log.Timber

/**
* Based on:
* https://proandroiddev.com/jwt-authentication-and-refresh-token-in-android-with-retrofit-interceptor-authenticator-da021f7f7534
*/
class ApiCallContext private constructor(
    private val moshi: Moshi,
    private val timeoutMs: Long,
    private val baseResponseAdapter: JsonAdapter<BaseResponse>,
) {
    @OptIn(ExperimentalStdlibApi::class)
    constructor(moshi: Moshi, timeoutSec: Long) : this(moshi, timeoutSec * 1000L, moshi.adapter<BaseResponse>())

    suspend fun <T> call(call: suspend () -> Response<T>): ApiResult<T> =
        withContext(Dispatchers.IO) {
            withTimeoutOrNull(timeMillis = this@ApiCallContext.timeoutMs) {
                try {
                    val response = call()
                    if (response.isSuccessful) {
                        response.body()?.let { content ->
                            ApiResult.Success(content)
                        }
                    } else {
                        response.errorBody()?.let { error ->
                            error.close()
                            val baseResponse =
                                this@ApiCallContext.baseResponseAdapter.fromJson(error.toString())
                            if (baseResponse != null) {
                                ApiResult.Failure(baseResponse.status, baseResponse.reason)
                            } else {
                                ApiResult.Failure(
                                    RestStatusInt.INTERNAL_SERVER_ERROR_500,
                                    "Unable to read error response",
                                )
                            }
                        }
                    }
                } catch (e: Exception) {
                    Timber.tag(TAG).e("$e")
                    ApiResult.Failure(RestStatusInt.BAD_REQUEST_400, e.message ?: e.toString())
                }
            } ?: ApiResult.Failure(
                RestStatusInt.REQUEST_TIMEOUT_408,
                RestStatusString.REQUEST_TIMEOUT_408,
            )
        }

    fun <T> callAsFlow(call: suspend () -> Response<T>): Flow<ApiResult<T>> = flow {
        emit(ApiResult.Loading)

        withTimeoutOrNull(timeMillis = this@ApiCallContext.timeoutMs) {
            val response = call()

            try {
                if (response.isSuccessful) {
                    response.body()?.let { content ->
                        emit(ApiResult.Success(content))
                    }
                } else {
                    response.errorBody()?.let { error ->
                        error.close()
                        val baseResponse = this@ApiCallContext.baseResponseAdapter.fromJson(error.toString())
                        emit(if (baseResponse != null) {
                            ApiResult.Failure(baseResponse.status, baseResponse.reason)
                        } else {
                            ApiResult.Failure(RestStatusInt.INTERNAL_SERVER_ERROR_500, "Unable to read error response")
                        })
                    }
                }
            } catch (e: Exception) {
                emit(ApiResult.Failure(RestStatusInt.BAD_REQUEST_400, e.message ?: e.toString()))
            }
        } ?: emit(ApiResult.Failure(RestStatusInt.REQUEST_TIMEOUT_408, RestStatusString.REQUEST_TIMEOUT_408))
    }.flowOn(Dispatchers.IO)

    companion object {
        @JvmStatic private val TAG = ApiCallContext::class.java.simpleName
    }
}
