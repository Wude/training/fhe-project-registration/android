@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.converter

import androidx.room.TypeConverter
import java.nio.ByteBuffer
import java.math.BigDecimal

class BigDecimalConverter {

    @TypeConverter
    fun toBigDecimal(value: String?): BigDecimal? = if (value == null) null else BigDecimal(value)

    @TypeConverter
    fun fromBigDecimal(value: BigDecimal?): String? = value?.toString()
}
