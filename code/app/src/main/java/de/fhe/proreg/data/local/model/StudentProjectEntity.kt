package de.fhe.proreg.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.ColumnInfo
import de.fhe.proreg.data.network.model.ProjectType
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDate
import java.util.UUID

@Entity
data class StudentProjectEntity(
    @PrimaryKey(autoGenerate = false) val projectID: UUID,
    var studentID: UUID,
    var studentEmail: String,
    var projectType: ProjectType,
    var title: String,
    var supervisingTutor1ID: UUID,
    var supervisingTutor1Email: String,
    var supervisingTutor2ID: UUID? = null,
    var supervisingTutor2Email: String? = null,
    val creationInstant: Instant = Instant.now(),
    var modificationInstant: Instant = Instant.now(),
    var grade: BigDecimal? = null,
    var deadline: Instant? = null,
    var supervisingTutor1Accepted: Boolean = false,
    var supervisingTutor2Accepted: Boolean = false,
    var studentSignature: String? = null,
    var supervisingTutor1Signature: String? = null,
    var supervisingTutor2Signature: String? = null,
)
