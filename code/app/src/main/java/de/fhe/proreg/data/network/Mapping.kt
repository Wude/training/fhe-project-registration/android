package de.fhe.proreg.data.network

import de.fhe.proreg.data.local.model.StudentProjectEntity
import de.fhe.proreg.data.local.model.StudentProjectDocumentEntity
import de.fhe.proreg.data.local.model.UserEntity
import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.data.network.model.StudentProjectDocument
import de.fhe.proreg.data.network.model.User

fun UserEntity.fromEntity(): User =
    User(
        userID = userID,
        email = email,
        userType = userType,
        admin = admin,
        facultyBoardOfExaminers = facultyBoardOfExaminers,
        facultyAcronyms = null,
        departmentAcronyms = null,
    )

fun User.toEntity(): UserEntity =
    UserEntity(
        userID = userID,
        email = email,
        userType = userType,
        admin = admin,
        facultyBoardOfExaminers = facultyBoardOfExaminers,
        // facultyAcronyms = facultyAcronyms,
        // departmentAcronyms = departmentAcronyms,
    )

fun StudentProjectEntity.fromEntity(): StudentProject =
    StudentProject(
        studentID=studentID,
        studentEmail=studentEmail,
        projectType=projectType,
        title=title,
        supervisingTutor1ID=supervisingTutor1ID,
        supervisingTutor1Email=supervisingTutor1Email,
        supervisingTutor2ID=supervisingTutor2ID,
        supervisingTutor2Email=supervisingTutor2Email,
        projectID=projectID,
        creationInstant=creationInstant,
        modificationInstant=modificationInstant,
        grade=grade,
        deadline=deadline,
        supervisingTutor1Accepted=supervisingTutor1Accepted,
        supervisingTutor2Accepted=supervisingTutor2Accepted,
        studentSignature=studentSignature,
        supervisingTutor1Signature=supervisingTutor1Signature,
        supervisingTutor2Signature=supervisingTutor2Signature,
    )

fun StudentProject.toEntity(): StudentProjectEntity =
    StudentProjectEntity(
        studentID=studentID,
        studentEmail=studentEmail,
        projectType=projectType,
        title=title,
        supervisingTutor1ID=supervisingTutor1ID,
        supervisingTutor1Email=supervisingTutor1Email,
        supervisingTutor2ID=supervisingTutor2ID,
        supervisingTutor2Email=supervisingTutor2Email,
        projectID=projectID,
        creationInstant=creationInstant,
        modificationInstant=modificationInstant,
        grade=grade,
        deadline=deadline,
        supervisingTutor1Accepted=supervisingTutor1Accepted,
        supervisingTutor2Accepted=supervisingTutor2Accepted,
        studentSignature=studentSignature,
        supervisingTutor1Signature=supervisingTutor1Signature,
        supervisingTutor2Signature=supervisingTutor2Signature,
    )

fun StudentProjectDocumentEntity.fromEntity(): StudentProjectDocument =
    StudentProjectDocument(
        projectID = projectID,
        title=title,
        content=content,
        projectDocumentID=projectDocumentID,
        creationInstant=creationInstant,
        modificationInstant=modificationInstant,
    )

fun StudentProjectDocument.toEntity(): StudentProjectDocumentEntity =
    StudentProjectDocumentEntity(
        projectID = projectID,
        title=title,
        content=content,
        projectDocumentID=projectDocumentID,
        creationInstant=creationInstant,
        modificationInstant=modificationInstant,
    )
