package de.fhe.proreg.ui.screen.user


import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.usecase.LogoutUseCase

class SettingsViewModel(
    private val logoutUseCase: LogoutUseCase,
): ViewModel() {

    suspend fun logout() = logoutUseCase()
}
