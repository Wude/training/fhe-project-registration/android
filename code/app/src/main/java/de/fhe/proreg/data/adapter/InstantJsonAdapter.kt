@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.adapter

import de.fhe.proreg.util.format
import de.fhe.proreg.util.parseInstant
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.time.Instant

class InstantJsonAdapter {

    @FromJson
    fun fromJson(json: String): Instant = json.parseInstant()

    @ToJson
    fun toJson(value: Instant): String = value.format()
}
