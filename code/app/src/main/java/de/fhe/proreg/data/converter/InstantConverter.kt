@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.converter

import androidx.room.TypeConverter
import java.time.Instant

class InstantConverter {

    @TypeConverter
    fun toInstant(value: Long?): Instant? = if (value == null) null else Instant.ofEpochSecond(value)

    @TypeConverter
    fun fromInstant(value: Instant?): Long? = value?.epochSecond
}
