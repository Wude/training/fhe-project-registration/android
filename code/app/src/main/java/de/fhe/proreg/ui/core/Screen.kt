package de.fhe.proreg.ui.core

import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import de.fhe.proreg.di.koinBaseDoubleModule
import de.fhe.proreg.di.koinViewModelModule
import org.koin.core.context.startKoin
import timber.log.Timber

abstract class Screen(
    val icon: ImageVector,
    route: String,
    vararg arguments: NamedNavArgument,
) {
    abstract val title: String
    var route: String = route; private set
    protected var destinationTemplate: String = route; private set
    val arguments: List<NamedNavArgument> = arguments.toList()

    init {
        for (argument in arguments) {
            this.route += "/{" + argument.name + "}"
            this.destinationTemplate += "/"
            when (argument.argument.type) {
                NavType.BoolType -> this.destinationTemplate += "%b"
                NavType.FloatType -> this.destinationTemplate += "%f"
                NavType.LongType -> this.destinationTemplate += "%d"
                NavType.StringType -> this.destinationTemplate += "%s"
                else -> this.destinationTemplate += "%s"
            }
        }
    }
}
