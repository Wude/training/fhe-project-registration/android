package de.fhe.proreg.data

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import de.fhe.proreg.data.network.model.Session
import de.fhe.proreg.data.network.model.Credentials
import de.fhe.proreg.data.network.model.User
import de.fhe.proreg.data.network.model.UserType
import de.fhe.proreg.util.toUuid
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.util.UUID

class PreferencesManagerProper constructor(
    applicationContext: Context
): PreferencesManager {
    private val preferences = EncryptedSharedPreferences.create(
        "fhe_proreg_app.prefs",
        MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
        applicationContext,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM,
    )

    override var useDarkTheme: Boolean
        get() = preferences.getUseDarkTheme()
        set(value) { preferences.edit().setUseDarkTheme(value).apply() }

    override val userID: UUID?; get() = preferences.getUserID()
    override val userType: UserType?; get() = preferences.getUserType()
    override val isAdmin: Boolean; get() = preferences.getIsAdmin()
    override val isFacultyBoardOfExaminers: Boolean; get() = preferences.getIsFacultyBoardOfExaminers()

    override val email: String?; get() = preferences.getEmail()
    override val password: String?; get() = preferences.getPassword()
    override val accessToken: String?; get() = preferences.getAccessToken()

    override var credentials: Credentials?
        get() {
            val email = preferences.getEmail()
            val password = preferences.getPassword()

            return if (email != null && password != null)
                Credentials(email, password) else null
        }
        set(value) {
            preferences.edit()
                .setEmail(value?.email)
                .setPassword(value?.password)
                .apply()
        }

    override var session: Session?
        get() {
            val accessToken = preferences.getAccessToken()
            val userID = preferences.getUserID()
            val email = preferences.getEmail()
            val userType = preferences.getUserType()
            val isAdmin = preferences.getIsAdmin()
            val isFacultyBoardOfExaminers = preferences.getIsFacultyBoardOfExaminers()

            return if (accessToken != null && userID != null && email != null && userType != null)
                    Session(accessToken, User(
                        userID, email, userType, isAdmin, isFacultyBoardOfExaminers, listOf(), listOf()))
                else null
        }
        set(value) {
            preferences.edit()
                .setAccessToken(value?.accessToken)
                .setUserID(value?.user?.userID)
                .setUserType(value?.user?.userType)
                .setIsAdmin(value?.user?.admin ?: false)
                .setIsFacultyBoardOfExaminers(value?.user?.facultyBoardOfExaminers ?: false)
                .apply()
            _sessionAsState.value = value
        }

    private var _sessionAsState = mutableStateOf(session)
    override val sessionAsState: State<Session?> = _sessionAsState

    companion object {
        @JvmStatic private val TAG = PreferencesManager::class.java.simpleName
        private const val KEY_USE_DARK_THEME = "useDarkTheme"
        private const val KEY_USER_ID = "userId"
        private const val KEY_USER_TYPE = "userType"
        private const val KEY_IS_ADMIN = "isAdmin"
        private const val KEY_IS_FBOE = "isFacultyBoardOfExaminers"
        private const val KEY_EMAIL = "email"
        private const val KEY_PASSWORD = "password"
        private const val KEY_ACCESS_TOKEN = "accessToken"

        private fun SharedPreferences.getUseDarkTheme(): Boolean { return this.getBoolean(KEY_USE_DARK_THEME, false) }
        private fun Editor.setUseDarkTheme(useDarkTheme: Boolean): Editor { return this.putBoolean(KEY_USE_DARK_THEME, useDarkTheme) }

        private fun SharedPreferences.getUserID(): UUID? { return this.getString(KEY_USER_ID, null).toUuid() }
        private fun Editor.setUserID(userId: UUID?): Editor { return this.putString(KEY_USER_ID, userId?.toString()) }

        private fun SharedPreferences.getUserType(): UserType? { return UserType.tryValueOf(this.getString(KEY_USER_TYPE, null)) }
        private fun Editor.setUserType(userType: UserType?): Editor { return this.putString(KEY_USER_TYPE, userType?.code) }

        private fun SharedPreferences.getIsAdmin(): Boolean { return this.getBoolean(KEY_IS_ADMIN, false) }
        private fun Editor.setIsAdmin(isAdmin: Boolean): Editor { return this.putBoolean(KEY_IS_ADMIN, isAdmin) }

        private fun SharedPreferences.getIsFacultyBoardOfExaminers(): Boolean { return this.getBoolean(KEY_IS_FBOE, false) }
        private fun Editor.setIsFacultyBoardOfExaminers(isAdmin: Boolean): Editor { return this.putBoolean(KEY_IS_FBOE, isAdmin) }

        private fun SharedPreferences.getEmail(): String? { return this.getString(KEY_EMAIL, null) }
        private fun Editor.setEmail(email: String?): Editor { return this.putString(KEY_EMAIL, email) }

        private fun SharedPreferences.getPassword(): String? { return this.getString(KEY_PASSWORD, null) }
        private fun Editor.setPassword(password: String?): Editor { return this.putString(KEY_PASSWORD, password) }

        private fun SharedPreferences.getAccessToken(): String? { return this.getString(KEY_ACCESS_TOKEN, null) }
        private fun Editor.setAccessToken(accessToken: String?): Editor { return this.putString(KEY_ACCESS_TOKEN, accessToken) }
    }
}
