package de.fhe.proreg.ui.core.input.validators

class EmailValidator(
    private val errorText: String = "E-Mail-Adresse ungültig",
): Validator<String>() {
    override fun validate(value: String): String? =
        if (value.isBlank() || value.matches(emailRegex)) null else errorText

    companion object {
        private val emailRegex = "[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\\.[a-z][a-z]+".toRegex()
    }
}
