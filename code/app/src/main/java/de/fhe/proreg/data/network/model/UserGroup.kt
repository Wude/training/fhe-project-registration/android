package de.fhe.proreg.data.network.model

/**
* The groups a user may belong to.
*/
object UserGroup {

    /**
    * A student.
    */
    const val STUDENT = "STU"

    /**
    * A lecturer.
    */
    const val LECTURER = "LEC"

    /**
    * A lecturer who is also a member of the faculty board of examiners.
    */
    const val FACULTY_BOARD_OF_EXAMINERS = "FBE"

    /**
    * A staff member of the faculty secreteriat.
    */
    const val FACULTY_SECRETARIAT_STAFF = "FSS"

    /**
    * A staff member of the examination authority.
    */
    const val EXAMINATION_AUTHORITY_STAFF = "EAS"

    /**
    * An administrator.
    */
    const val ADMINISTRATOR = "ADM"
}
