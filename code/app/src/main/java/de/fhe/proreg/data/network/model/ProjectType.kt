package de.fhe.proreg.data.network.model

enum class ProjectType(val code: String) {
    /**
    * A Bachelor's project.
    */
    BACHELORS_PROJECT("BAP"),

    /**
    * A Bachelor's thesis.
    */
    BACHELORS_THESIS("BAT"),

    /**
    * A Master's project.
    */
    MASTERS_PROJECT("MAP"),

    /**
    * A Master's thesis.
    */
    MASTERS_THESIS("MAT");

    companion object {

        val valuesByCode = HashMap<String, ProjectType>()

        init {
            val projectTypeValues = ProjectType.values()

            for (projectType in projectTypeValues) {
                valuesByCode.put(projectType.code, projectType);
                valuesByCode.put(projectType.name, projectType);
            }
        }

        fun tryValueOf(idOrCode: String?, defaultValue: ProjectType): ProjectType = valuesByCode.get(idOrCode) ?: defaultValue
        fun tryValueOf(idOrCode: String?): ProjectType? = valuesByCode.get(idOrCode)
    }
}
