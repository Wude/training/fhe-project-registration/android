package de.fhe.proreg.data.network

import android.util.Log
import com.squareup.moshi.Moshi
import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.di.addCommonInterceptors
import de.fhe.proreg.di.baseUrl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.flow.singleOrNull
import kotlinx.coroutines.runBlocking
import java.io.IOException
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.Request
import okhttp3.Route
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

class TokenRefreshAuthenticator(
    private val moshi: Moshi,
    private val prefs: PreferencesManager,
) : Authenticator {
    private val httpClient = OkHttpClient.Builder()
        .connectTimeout(300, TimeUnit.SECONDS)
        .readTimeout(300, TimeUnit.SECONDS)
        .writeTimeout(300, TimeUnit.SECONDS)
        .addCommonInterceptors()
        .build()

    private val userApi = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(httpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
        .build()
        .create(UserApi::class.java)

    override fun authenticate(route: Route?, response: Response): Request? =
        response.createRequestWithAuth()

    private fun Response.createRequestWithAuth(): Request? =
        runBlocking(Dispatchers.IO) {
            try {
                val credentials = prefs.credentials
                if (credentials != null) {
                    val loginResponse = userApi.login(credentials)
                    if (loginResponse.isSuccessful) {
                        prefs.session = loginResponse.body()
                        request.setAuth()
                    } else {
                        null
                    }
                } else {
                    null
                }
            } catch (error: Throwable) {
                Timber.tag(TAG).e(error.toString())
                null
            }
        }

    private fun Request.setAuth(): Request =
        newBuilder().header(AUTH, "Bearer ${this@TokenRefreshAuthenticator.prefs.accessToken}").build()

    private val Response.retryCount: Int
        get() {
            var currentResponse = priorResponse
            var result = 0
            while (currentResponse != null) {
                result++
                currentResponse = currentResponse.priorResponse
            }
            return result
        }

    companion object {
        @JvmStatic private val TAG = TokenRefreshAuthenticator::class.java.simpleName
        private const val AUTH = "Authorization"
    }

}
