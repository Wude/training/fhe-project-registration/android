package de.fhe.proreg.data

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import de.fhe.proreg.data.network.model.Session
import de.fhe.proreg.data.network.model.Credentials
import de.fhe.proreg.data.network.model.UserType
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.util.UUID

class PreferencesManagerStub: PreferencesManager {
    private var _userID: UUID? = null
    private val _userType: UserType? = null
    private val _isAdmin: Boolean = false
    private val _isFacultyBoardOfExaminers: Boolean = false
    private var _email: String? = null
    private var _password: String? = null
    private var _accessToken: String? = null

    private var _credentials: Credentials? = null
    private var _session: Session? = null

    override var useDarkTheme: Boolean = true

    override val userID: UUID? = _userID
    override val userType: UserType? = _userType
    override val isAdmin: Boolean = _isAdmin
    override val isFacultyBoardOfExaminers: Boolean = _isFacultyBoardOfExaminers
    override val email: String? = _email
    override val password: String? = _password
    override val accessToken: String? = _accessToken

    override var credentials: Credentials? = _credentials
    override var session: Session?
        get() = _session
        set(value) { _session = value; _sessionAsState.value = value }

    private var _sessionAsState = mutableStateOf(session)
    override val sessionAsState: State<Session?> = _sessionAsState
}
