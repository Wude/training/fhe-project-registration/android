package de.fhe.proreg.ui.core.input

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import de.fhe.proreg.ui.core.input.validators.Validator

abstract class Input<T>(
    val label: String,
    val value: T,
    val onValueChange: (value: T) -> Unit,
    vararg validators: Validator<T>?,
) {
    val validators: List<Validator<T>> = validators.filterNotNull().toList()
    protected val hasError: Boolean; get() = errorTextLines.value.any()
    protected lateinit var errorTextLines: MutableState<List<String>>

    fun validate(value: T): Boolean {
        errorTextLines.value = validators.mapNotNull {
            it.validate(value)
        }
        return !hasError
    }

    fun validate() = validate(value)
}
