package de.fhe.proreg.data.local

import androidx.room.Dao
import androidx.room.Database
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.OnConflictStrategy
import de.fhe.proreg.data.adapter.UUIDJsonAdapter
import de.fhe.proreg.data.local.model.UserEntity
import de.fhe.proreg.data.network.model.UserType
import kotlinx.coroutines.flow.Flow
import java.util.UUID

@Dao
interface UserAccess {
    @Query("select * from UserEntity")
    fun getAll(): Flow<List<UserEntity>>

    @Query("select * from UserEntity where userType = :userType")
    fun getByType(userType: UserType): Flow<List<UserEntity>>

    @Query("select * from UserEntity where userID = :userID")
    fun get(userID: UUID?): Flow<UserEntity?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(project: UserEntity)

    @Delete
    suspend fun remove(vararg users: UserEntity)

    @Query("delete from UserEntity")
    suspend fun removeAll(): Unit
}
