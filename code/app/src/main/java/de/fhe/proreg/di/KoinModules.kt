package de.fhe.proreg.di

import androidx.lifecycle.SavedStateHandle
import androidx.room.Room
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import de.fhe.proreg.data.ApiCallContext
import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.data.ProjectRepository
import de.fhe.proreg.data.UserRepository
import de.fhe.proreg.data.adapter.UserTypeJsonAdapter
import de.fhe.proreg.data.adapter.ProjectTypeJsonAdapter
import de.fhe.proreg.data.adapter.ProjectStatusJsonAdapter
import de.fhe.proreg.data.adapter.UUIDJsonAdapter
import de.fhe.proreg.data.adapter.LocalDateJsonAdapter
import de.fhe.proreg.data.adapter.InstantJsonAdapter
import de.fhe.proreg.data.adapter.BigDecimalJsonAdapter
import de.fhe.proreg.data.adapter.GenericCollectionAdapterFactory
import de.fhe.proreg.data.local.Db
import de.fhe.proreg.data.local.StudentProjectAccess
import de.fhe.proreg.data.local.StudentProjectDocumentAccess
import de.fhe.proreg.data.network.UserApi
import de.fhe.proreg.data.network.ProjectApi
import de.fhe.proreg.data.network.AuthorizationInterceptor
import de.fhe.proreg.data.network.TokenRefreshAuthenticator
import de.fhe.proreg.data.PreferencesManagerProper
import de.fhe.proreg.data.PreferencesManagerStub
import de.fhe.proreg.data.ProjectRepositoryProper
import de.fhe.proreg.data.ProjectRepositoryFake
import de.fhe.proreg.data.UserRepositoryProper
import de.fhe.proreg.data.UserRepositoryFake
import de.fhe.proreg.data.local.UserAccess
import de.fhe.proreg.data.network.model.ProjectType
import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.ui.core.ScreenEnum
import de.fhe.proreg.ui.screen.Screens
import de.fhe.proreg.ui.screen.user.LoginViewModel
import de.fhe.proreg.ui.screen.user.SettingsViewModel
import de.fhe.proreg.ui.screen.student.StudentCreateProjectViewModel
import de.fhe.proreg.ui.screen.student.StudentViewProjectViewModel
import de.fhe.proreg.ui.screen.student.StudentViewModel
import de.fhe.proreg.usecase.CreateProjectUseCase
import de.fhe.proreg.usecase.CreateProjectUseCaseProper
import de.fhe.proreg.usecase.DownloadLecturersUseCase
import de.fhe.proreg.usecase.DownloadRelatedProjectsUseCase
import de.fhe.proreg.usecase.GetLecturersUseCase
import de.fhe.proreg.usecase.GetProjectsUseCase
import de.fhe.proreg.usecase.LoginUseCase
import de.fhe.proreg.usecase.LogoutUseCase
import de.fhe.proreg.usecase.GetProjectDocumentUseCase
import de.fhe.proreg.usecase.GetProjectUseCase
import kotlinx.coroutines.runBlocking
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.UUID

// const val baseUrl = "http://192.168.178.89/api/"
// const val baseUrl = "http://192.168.178.89:8080/api/"
// const val baseUrl = "http://wude-deb/api/"
const val baseUrl = "http://wude-deb:8080/api/"

fun OkHttpClient.Builder.addCommonInterceptors(): OkHttpClient.Builder {
    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    this.addInterceptor(loggingInterceptor)
    this.addInterceptor {
        val request = it.request().newBuilder()
            .addHeader("User-Agent", "FHE Project Rgistration (Android v0.0.1)")
            .build()
        it.proceed(request)
    }
    return this
}

val koinBaseModule = module {
    single<ScreenEnum> { Screens }
    single<PreferencesManager> { PreferencesManagerProper(androidContext()) }
}

val koinBaseDoubleModule = module {
    single<ScreenEnum> { Screens }
    single<PreferencesManager> { PreferencesManagerStub() }
}

val koinRoomModule = module {
    single<Db> {
        Room.databaseBuilder(
            androidApplication(),
            Db::class.java, "fhe_proreg_app-db" )
            .fallbackToDestructiveMigration()
            .build()
    }

    single<UserAccess> { get<Db>().userAccess() }
    single<StudentProjectAccess> { get<Db>().studentProjectAccess() }
    single<StudentProjectDocumentAccess> { get<Db>().studentProjectDocumentAccess() }
}

val koinDataModule = module {
    single<UserRepository> { UserRepositoryProper(get(), get(), get(), get()) }
    single<ProjectRepository> { ProjectRepositoryProper(get(), get(), get(), get()) }
}

val koinDataDoubleModule = module {
    single<UserRepository> { UserRepositoryFake() }
    single<ProjectRepository> { ProjectRepositoryFake() }
}

val koinNetworkModule = module {
    single<Moshi> {
        Moshi.Builder()
            .add(GenericCollectionAdapterFactory(ArrayList::class.java) { ArrayList() })
            .add(UserTypeJsonAdapter())
            .add(ProjectTypeJsonAdapter())
            .add(ProjectStatusJsonAdapter())
            .add(UUIDJsonAdapter())
            .add(InstantJsonAdapter())
            .add(LocalDateJsonAdapter())
            .add(BigDecimalJsonAdapter())
            .addLast(KotlinJsonAdapterFactory())
            .build()
    }

    single { ApiCallContext(get(), 5L) }

    single<UserApi> {
        val moshi = get<Moshi>()

        val httpClient = OkHttpClient.Builder()
            .connectTimeout(300, TimeUnit.SECONDS)
            .readTimeout(300, TimeUnit.SECONDS)
            .writeTimeout(300, TimeUnit.SECONDS)
            .addCommonInterceptors()
            .addInterceptor(AuthorizationInterceptor(get()))
            .authenticator(TokenRefreshAuthenticator(get(), get()))
            .build()

        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(httpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
            .build()
            .create(UserApi::class.java)
    }

    single<ProjectApi> {
        val moshi = get<Moshi>()

        val httpClient = OkHttpClient.Builder()
            .connectTimeout(300, TimeUnit.SECONDS)
            .readTimeout(300, TimeUnit.SECONDS)
            .writeTimeout(300, TimeUnit.SECONDS)
            .addCommonInterceptors()
            .addInterceptor(AuthorizationInterceptor(get()))
            .authenticator(TokenRefreshAuthenticator(get(), get()))
            .build()

        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(httpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
            .build()
            .create(ProjectApi::class.java)
    }
}

val koinUseCaseModule = module {
    single { LoginUseCase(get<UserRepository>()::login) }
    single { LogoutUseCase(get<UserRepository>()::logout) }
    single { DownloadLecturersUseCase(get<UserRepository>()::downloadLecturers) }
    single<CreateProjectUseCase> { CreateProjectUseCaseProper(get()) }
    single { GetLecturersUseCase(get<UserRepository>()::getLecturers) }
    single { GetProjectsUseCase(get<ProjectRepository>()::getProjects) }
    single { DownloadRelatedProjectsUseCase(get<ProjectRepository>()::downloadRelatedProjects) }
    single { GetProjectUseCase(get<ProjectRepository>()::getProject) }
    single { GetProjectDocumentUseCase(get<ProjectRepository>()::getProjectDocument) }
}

val koinViewModelModule = module {
    viewModel { LoginViewModel(get(), get()) }
    viewModel { SettingsViewModel(get()) }
    viewModel { StudentViewModel(get(), get(), get()) }
    viewModel { StudentCreateProjectViewModel(get(), get(), get(), get(), get()) }
    viewModel { StudentViewProjectViewModel(get(), get()) }
}

val koinViewModelDoubleModule = module {
    viewModel { LoginViewModel(SavedStateHandle(), get()) }
    viewModel { SettingsViewModel(get()) }
    viewModel { StudentViewModel(SavedStateHandle(), get(), get()) }
    viewModel { StudentCreateProjectViewModel(SavedStateHandle(), get(), get(), get(), get()) }
    viewModel { StudentViewProjectViewModel(SavedStateHandle(), get()) }
}

class PreviewData: KoinComponent {
    private val projectRepository by inject<ProjectRepository>()

    fun prepare() {
        // runBlocking(Dispatchers.IO) {
        runBlocking {
            projectRepository.addProject(
                StudentProject(
                    projectID = UUID.fromString("6510aab9-9108-08db-db56-775746567316"),
                    studentID = UUID.fromString("612bc797-83ef-efa7-a782-775746567316"),
                    studentEmail = "stefan.woyde@fh-erfurt.de",
                    projectType = ProjectType.MASTERS_PROJECT,
                    title = "Test-MA-Projekt",
                    supervisingTutor1ID = UUID.fromString("612d3ea1-a0f0-f0a7-a7fc-775746569316"),
                    supervisingTutor1Email = "steffen.avemarg@fh-erfurt.de",
                )
            )
            projectRepository.addProject(
                StudentProject(
                    projectID = UUID.fromString("6510aaf1-9462-62a7-a770-775746567316"),
                    studentID = UUID.fromString("612bc797-83ef-efa7-a782-775746567316"),
                    studentEmail = "stefan.woyde@fh-erfurt.de",
                    projectType = ProjectType.MASTERS_THESIS,
                    title = "Test-MA-Abschlussarbeit",
                    supervisingTutor1ID = UUID.fromString("612d3ea1-a0f0-f0a7-a7fc-775746569316"),
                    supervisingTutor1Email = "steffen.avemarg@fh-erfurt.de",
                    supervisingTutor2ID = UUID.fromString("6510b80d-5c6a-6a0c-0cce-775746567316"),
                    supervisingTutor2Email = "kay.guertzig@fh-erfurt.de",
                )
            )
        }
    }
}
