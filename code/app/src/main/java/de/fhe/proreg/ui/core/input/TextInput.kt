package de.fhe.proreg.ui.core.input

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.TextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.FocusState
import androidx.compose.ui.focus.focusProperties
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import de.fhe.proreg.ui.core.input.validators.Validator

open class TextInput(
    label: String,
    value: String,
    onValueChange: (value: String) -> Unit,
    vararg validators: Validator<String>?,
): Input<String>(
    label = label,
    value = value,
    onValueChange = onValueChange,
    //validators = validators.toList(),
    validators = validators,
) {
    @Composable
    fun show(
        modifier: Modifier = Modifier,
        placeholder: String? = null,
        enabled: Boolean = true,
        readOnly: Boolean = false,
        singleLine: Boolean = false,
        maxLines: Int = Int.MAX_VALUE,
        focusChanged: ((focus: FocusState) -> Unit)? = null,
        focusRequester: FocusRequester = FocusRequester(),
        leadingIcon: @Composable (() -> Unit)? = null,
        trailingIcon: @Composable (() -> Unit)? = null,
        imeAction: ImeAction = ImeAction.Next,
        keyboardType: KeyboardType = KeyboardType.Text,
        keyboardActions: KeyboardActions = KeyboardActions.Default,
    ): TextInput {
        errorTextLines = remember { mutableStateOf(listOf<String>()) }

        Column(modifier = modifier) {
            TextField(
                value = value,
                onValueChange = {
                    onValueChange(it)
                    validate(it)
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .focusProperties { right }
                    .focusRequester(focusRequester)
                    .onFocusChanged { focusChanged?.invoke(it) },
                enabled = enabled,
                readOnly = readOnly,
                label = { Text(label) },
                placeholder = {
                    if (placeholder != null) {
                        Text(placeholder)
                    }
                },
                leadingIcon = leadingIcon,
                trailingIcon = trailingIcon,
                isError = hasError,
                visualTransformation = VisualTransformation.None,
                keyboardOptions = KeyboardOptions(
                    capitalization = KeyboardCapitalization.None,
                    autoCorrect = false,
                    imeAction = imeAction,
                    keyboardType = keyboardType,
                ),
                keyboardActions = keyboardActions,
                singleLine = singleLine,
                maxLines = maxLines,
                colors = OutlinedTextFieldDefaults.colors(),
            )

            if (hasError) {
                Text(
                    text = errorTextLines.value.joinToString("\n"),
                    modifier = Modifier.padding(
                        horizontal = 8.dp,
                        vertical = 4.dp
                    ),
                    color = MaterialTheme.colorScheme.error,
                )
            }
        }

        return this
    }
}
