package de.fhe.proreg.ui.screen.boa

import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material.icons.filled.Group
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import de.fhe.proreg.ui.core.DrawerAccessState
import de.fhe.proreg.ui.core.PrepareFrame
import de.fhe.proreg.ui.core.Preview
import de.fhe.proreg.ui.core.ScreenWithoutArguments
import de.fhe.proreg.ui.core.TopBarMenuItem

object BoardOfExaminersScreen: ScreenWithoutArguments(
    icon = Icons.Filled.Group,
    route = "board-of-examiners",
    displayRank = 3,
) {
    override val title = "Prüfungsausschuss"

    @Composable
    override fun Show() {
        PrepareFrame(
            BoardOfExaminersScreen.title,
            DrawerAccessState.BACK,
            TopBarMenuItem(
                icon = Icons.Filled.FavoriteBorder,
                title = "Favorite",
                action = { },
            ),
            TopBarMenuItem(
                icon = Icons.Filled.Search,
                title = "Suche",
                action = { },
                isInOverflow = true,
            ),
            TopBarMenuItem(
                title = "Test",
                action = { },
            ),
        )

        Column {
            Text(text = BoardOfExaminersScreen.title)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun Preview() {
    BoardOfExaminersScreen.Preview()
}
