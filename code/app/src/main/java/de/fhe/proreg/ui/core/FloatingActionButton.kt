package de.fhe.proreg.ui.core

import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp

@Composable
fun BoxScope.FloatingActionButton(
    icon: ImageVector,
    title: String? = null,
    onClick: () -> Unit,
) {
    if (title == null) {
        FloatingActionButton(
            modifier = Modifier
                .padding(all = 16.dp)
                .align(alignment = Alignment.BottomEnd),
            onClick = onClick,
            shape = RoundedCornerShape(16.dp),
        ) {
            Icon(icon, null)
        }
    } else {
        ExtendedFloatingActionButton(
            modifier = Modifier
                .padding(all = 16.dp)
                .align(alignment = Alignment.BottomEnd),
            onClick = onClick,
            shape = RoundedCornerShape(16.dp),
            text = { Text(text = title) },
            icon = { Icon(imageVector = icon, contentDescription = title) }
        )
    }
}
