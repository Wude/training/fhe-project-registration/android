package de.fhe.proreg.ui.core

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.ime
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.material3.Scaffold
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.TopAppBarState
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.platform.LocalTextInputService
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.data.network.model.Session
import de.fhe.proreg.data.network.model.UserType
import de.fhe.proreg.di.koinBaseDoubleModule
import de.fhe.proreg.ui.screen.Screens
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import org.koin.androidx.compose.get
import org.koin.compose.koinInject
import org.koin.core.context.startKoin
import timber.log.Timber

val LocalNavController = staticCompositionLocalOf<NavHostController> { error("no nav controller set") }
@OptIn(ExperimentalMaterial3Api::class)
val LocalTopAppBarState = staticCompositionLocalOf<TopAppBarState> { error("no top bar state set") }
val LocalSnackbarState = staticCompositionLocalOf<SnackbarHostState> { error("no snackbar state set") }
val LocalPreferencesManager = staticCompositionLocalOf<PreferencesManager> { error("no app frame state set") }
val LocalUseDarkTheme = staticCompositionLocalOf<MutableState<Boolean>> { error("no theme state set") }

val LocalTitle = compositionLocalOf<MutableState<String>> { error("no title set") }
val LocalDrawerAccess = compositionLocalOf<MutableState<DrawerAccessState>> { error("no drawer access set") }
val LocalDrawerShortcuts = compositionLocalOf<MutableState<List<ScreenWithoutArguments>>> { error("no drawer shortcuts set") }
val LocalBottomShortcuts = compositionLocalOf<MutableState<List<ScreenWithoutArguments>>> { error("no bottom shortcuts set") }
val LocalMenuItems = compositionLocalOf<MutableState<List<TopBarMenuItem>>> { error("no menu items set") }
val LocalSession = compositionLocalOf<State<Session?>> { error("no session set") }

@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun AppFrame() {
    val prefs = koinInject<PreferencesManager>()
    val screens = koinInject<ScreenEnum>()

    val scope = rememberCoroutineScope()
    val navController = rememberNavController()
    val topAppBarState = rememberTopAppBarState()
    val snackbarState = remember { SnackbarHostState() }
    val drawerState = rememberDrawerState(DrawerValue.Closed)

    val title = rememberSaveable { mutableStateOf(screens.Start.title) }
    val drawerAccess = rememberSaveable { mutableStateOf(DrawerAccessState.ACCESSIBLE) }
    val drawerShortcuts = rememberSaveableList<ScreenWithoutArguments>(screens)
    val bottomShortcuts = rememberSaveableList<ScreenWithoutArguments>(screens)
    val menuItems = remember { mutableStateOf(listOf<TopBarMenuItem>()) }
    val session = remember { prefs.sessionAsState }
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry?.destination?.route
    val keyboardController = LocalSoftwareKeyboardController.current
    val keyboardVisible = WindowInsets.ime.getBottom(LocalDensity.current) > 0

    drawerShortcuts.value = screens.drawerShortcuts(prefs)
    bottomShortcuts.value = screens.bottomShortcuts(prefs)

    BackHandler(enabled = drawerState.isOpen) {
        scope.launch {
            drawerState.close()
        }
    }

    BackHandler(enabled = keyboardVisible) {
        scope.launch {
            keyboardController?.hide()
        }
    }

    CompositionLocalProvider(
        LocalNavController provides navController,
        LocalTopAppBarState provides topAppBarState,
        LocalSnackbarState provides snackbarState,
        LocalTitle provides title,
        LocalDrawerAccess provides drawerAccess,
        LocalDrawerShortcuts provides drawerShortcuts,
        LocalBottomShortcuts provides bottomShortcuts,
        LocalMenuItems provides menuItems,
        LocalSession provides session,
    ) {
        AppFrameContent(
            scope = scope,
            navController = navController,
            drawerState = drawerState,
            topAppBarState = topAppBarState,
            snackbarState = snackbarState,
            drawerAccess = drawerAccess,
            drawerShortcuts = drawerShortcuts.value,
            bottomShortcuts = bottomShortcuts.value,
            session = session.value,
            currentRoute = currentRoute,
        ) { innerPadding ->
            Navigation(
                screens,
                navController,
                startDestination = screens.Start.route,
                modifier = Modifier.padding(innerPadding),
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppFrameContent(
    scope: CoroutineScope,
    navController: NavHostController,
    drawerState: DrawerState,
    drawerAccess: State<DrawerAccessState>,
    topAppBarState: TopAppBarState,
    snackbarState: SnackbarHostState,
    drawerShortcuts: List<ScreenWithoutArguments>,
    bottomShortcuts: List<ScreenWithoutArguments>,
    session: Session?,
    currentRoute: String?,
    content: @Composable (PaddingValues) -> Unit,
) {
    Drawer(
        navController,
        currentRoute,
        drawerShortcuts,
        drawerState,
        session,
        scope,
    ) {
        Scaffold(
            modifier = Modifier.fillMaxSize(),
            snackbarHost = { SnackbarHost(snackbarState) },
            topBar = {
                TopBar(
                    navController,
                    topAppBarState,
                    drawerState,
                    drawerAccess,
                    scope,
                )
            },
            bottomBar = {
                BottomBar(
                    navController,
                    currentRoute,
                    bottomShortcuts,
                )
            },
        ) { innerPadding ->
            content(innerPadding)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun AppFramePreview() {
    startKoin {
        modules(koinBaseDoubleModule)
    }
    AppFrame()
}
