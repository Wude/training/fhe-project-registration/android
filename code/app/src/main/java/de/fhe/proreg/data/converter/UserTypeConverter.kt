@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.converter

import androidx.room.TypeConverter
import de.fhe.proreg.data.network.model.UserType

class UserTypeConverter {

    @TypeConverter
    fun toUserType(value: String?): UserType? = UserType.tryValueOf(value)

    @TypeConverter
    fun fromUserType(value: UserType?): String? = value?.code
}
