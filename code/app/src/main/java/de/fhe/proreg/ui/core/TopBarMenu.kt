package de.fhe.proreg.ui.core

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable

@Composable
fun TopBarMenu(
    menuItems: List<TopBarMenuItem>,
    isExpanded: Boolean,
    onSetExpanded: (Boolean) -> Unit,
) {
    menuItems.filter { !it.isInOverflow }.forEach {
        IconButton(onClick = it.action) {
            Icon(imageVector = it.icon!!, contentDescription = it.title)
        }
    }

    if (menuItems.any { it.isInOverflow }) {
        IconButton(onClick = { onSetExpanded(!isExpanded) }) {
            Icon(
                imageVector = Icons.Outlined.MoreVert,
                contentDescription = "Options",
            )
        }

        DropdownMenu(
            expanded = isExpanded,
            onDismissRequest = { onSetExpanded(false) },
        ) {
            menuItems.filter { it.isInOverflow }.forEach {
                DropdownMenuItem(
                    text = { Text(it.title) },
                    leadingIcon = {
                        if (it.icon != null) {
                            Icon(
                                imageVector = it.icon!!,
                                contentDescription = it.title,
                            )
                        }
                    },
                    onClick = {
                        onSetExpanded(false)
                        it.action()
                    }
                )
            }
        }
    }
}
