package de.fhe.proreg.ui.core.input

fun Iterable<Input<*>>.areValid() = !any { !it.validate() }
