package de.fhe.proreg.ui.screen.student

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.usecase.DownloadRelatedProjectsUseCase
import de.fhe.proreg.usecase.GetProjectsUseCase
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

@OptIn(SavedStateHandleSaveableApi::class)
class StudentViewModel(
    private val savedStateHandle: SavedStateHandle,
    private val downloadRelatedProjectsUseCase: DownloadRelatedProjectsUseCase,
    private val getProjectsUseCase: GetProjectsUseCase,
): ViewModel() {

    init {
        downloadRelatedProjects()
    }

    val projects: StateFlow<List<StudentProject>> = getProjectsUseCase().stateIn(
        initialValue = listOf(),
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000)
    )

    fun downloadRelatedProjects() { viewModelScope.launch { downloadRelatedProjectsUseCase() } }
}
