package de.fhe.proreg.ui.core.input.validators

/**
* A base for validators.
*
* @param T the type of values to validate.
*/
abstract class Validator<in T> {
    /**
    * Validate a [value] and return an explanation in case of an error.
    *
    * @param value the type of a member in this group.
    * @return An explanation in case of an error or else null.
    */
    abstract fun validate(value: T): String?
}
