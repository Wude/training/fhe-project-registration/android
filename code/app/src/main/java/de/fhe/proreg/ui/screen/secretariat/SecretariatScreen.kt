package de.fhe.proreg.ui.screen.secretariat

import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Work
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import de.fhe.proreg.ui.core.DrawerAccessState
import de.fhe.proreg.ui.core.PrepareFrame
import de.fhe.proreg.ui.core.ScreenWithoutArguments
import de.fhe.proreg.ui.core.TopBarMenuItem
import de.fhe.proreg.ui.screen.Screens

object SecretariatScreen: ScreenWithoutArguments(
    icon = Icons.Filled.Work,
    route = "secretariat",
    displayRank = 4,
) {
    override val title = "Sekretariat"

    @Composable
    override fun Show() {
        PrepareFrame(
            SecretariatScreen.title,
            DrawerAccessState.ACCESSIBLE,
            // Screens.withoutArguments,
            // Screens.withoutArguments,
        )

        Column {
            Text(text = SecretariatScreen.title)
        }
    }
}
