package de.fhe.proreg.ui.core.input.validators

class TextHasContentValidator(
    private val errorText: String = "Eingabe erforderlich",
): Validator<String?>() {
    override fun validate(value: String?): String? =
        if (!value.isNullOrEmpty() && value.isNotBlank()) null else errorText
}
