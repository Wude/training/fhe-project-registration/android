package de.fhe.proreg.usecase

import de.fhe.proreg.data.ProjectRepository
import de.fhe.proreg.data.network.model.ProjectType
import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.data.network.model.StudentProjectCreationRequest
import java.util.UUID

class CreateProjectUseCaseProper(
    private val projectRepository: ProjectRepository,
) : CreateProjectUseCase {
    override suspend operator fun invoke(
        projectType: ProjectType,
        studentID: UUID,
        studentEmail: String,
        supervisingTutor1ID: UUID,
        supervisingTutor1Email: String,
        supervisingTutor2ID: UUID?,
        supervisingTutor2Email: String?,
        title: String,
        studentSignature: String,
    ): UUID? {
        val project = StudentProjectCreationRequest(
            studentID = studentID,
            projectType = projectType,
            title = title,
            supervisingTutor1ID = supervisingTutor1ID,
            supervisingTutor2ID = supervisingTutor2ID,
            studentSignature = studentSignature,
        )
        val projectID = projectRepository.uploadProject(project)
        projectRepository.addProject(
            StudentProject(
                studentID = studentID,
                studentEmail = studentEmail,
                projectType = projectType,
                title = title,
                supervisingTutor1ID = supervisingTutor1ID,
                supervisingTutor1Email = supervisingTutor1Email,
                supervisingTutor2ID = supervisingTutor2ID,
                supervisingTutor2Email = supervisingTutor2Email,
                studentSignature = studentSignature,
            )
        )
        return projectID
    }
}

class RequestProjectDeadlineExtension() {
    operator fun invoke(projectID: UUID) {
    }
}

class CreateProjectDocument() {
    operator fun invoke(
        projectID: UUID,
        projectDocumentID: UUID?,
        title: String,
        content: ByteArray,
    ): UUID {
        return projectDocumentID ?: UUID.randomUUID()
    }
}
