@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.converter

import androidx.room.TypeConverter
import de.fhe.proreg.data.network.model.ProjectType

class ProjectTypeConverter {

    @TypeConverter
    fun toProjectType(value: String?): ProjectType? = ProjectType.tryValueOf(value)

    @TypeConverter
    fun fromProjectType(value: ProjectType?): String? = value?.code
}
