package de.fhe.proreg.data.network.model

import java.math.BigDecimal
import java.time.Instant
import java.util.UUID

data class StudentProjectCreationRequest(
    var studentID: UUID,
    var projectType: ProjectType,
    var title: String,
    var supervisingTutor1ID: UUID,
    var supervisingTutor2ID: UUID? = null,
    val projectID: UUID = UUID.randomUUID(),
    val creationInstant: Instant = Instant.now(),
    var modificationInstant: Instant = Instant.now(),
    var grade: BigDecimal? = null,
    var deadline: Instant? = null,
    var status: ProjectStatus = ProjectStatus.SUBMITTED,
    var supervisingTutor1Accepted: Boolean = false,
    var supervisingTutor2Accepted: Boolean = false,
    var studentSignature: String? = null,
    var supervisingTutor1Signature: String? = null,
    var supervisingTutor2Signature: String? = null,
)

data class StudentProjectCreationResponse(
    var projectID: UUID,
)

data class StudentProject(
    var studentID: UUID,
    var studentEmail: String,
    var projectType: ProjectType,
    var title: String,
    var supervisingTutor1ID: UUID,
    var supervisingTutor1Email: String,
    var supervisingTutor2ID: UUID? = null,
    var supervisingTutor2Email: String? = null,
    val projectID: UUID = UUID.randomUUID(),
    val creationInstant: Instant = Instant.now(),
    var modificationInstant: Instant = Instant.now(),
    var grade: BigDecimal? = null,
    var deadline: Instant? = null,
    var status: ProjectStatus = ProjectStatus.SUBMITTED,
    var supervisingTutor1Accepted: Boolean = false,
    var supervisingTutor2Accepted: Boolean = false,
    var studentSignature: String? = null,
    var supervisingTutor1Signature: String? = null,
    var supervisingTutor2Signature: String? = null,
)
