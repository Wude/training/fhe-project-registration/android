package de.fhe.proreg.ui.core

import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.listSaver
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.navigation.NavHostController
import timber.log.Timber

@Composable
fun PrepareFrame(
    title: String = "",
    drawerAccess: DrawerAccessState = DrawerAccessState.HIDDEN,
    // drawerShortcuts: List<ScreenWithoutArguments> = listOf(),
    // bottomShortcuts: List<ScreenWithoutArguments> = listOf(),
    vararg menuItems: TopBarMenuItem,
) {
    LocalTitle.current.value = title
    LocalDrawerAccess.current.value = drawerAccess
    // LocalDrawerShortcuts.current.value = drawerShortcuts
    // LocalBottomShortcuts.current.value = bottomShortcuts
    LocalMenuItems.current.value = menuItems.toList()
}

fun NavHostController.navigateUpTo(destination: String, saveState: Boolean = true) {
    Timber.d("NavHostController.navigateUpTo: $destination, saveState = $saveState")
    this.navigate(destination) {
        this@navigateUpTo.graph.startDestinationRoute?.let { route ->
            popUpTo(route) {
                this.saveState = saveState
                this.inclusive = true
            }
        }
        this.launchSingleTop = true
        this.restoreState = true
    }
}

fun NavHostController.navigateTo(destination: String) {
    Timber.d("NavHostController.navigateTo: $destination")
    this.navigate(destination)
}
