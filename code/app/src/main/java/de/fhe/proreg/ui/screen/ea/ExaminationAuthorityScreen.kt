package de.fhe.proreg.ui.screen.ea

import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Work
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import de.fhe.proreg.ui.core.DrawerAccessState
import de.fhe.proreg.ui.core.PrepareFrame
import de.fhe.proreg.ui.core.Preview
import de.fhe.proreg.ui.core.ScreenWithoutArguments

object ExaminationAuthorityScreen: ScreenWithoutArguments(
    icon = Icons.Filled.Work,
    route = "examination-authority",
    displayRank = 5,
) {
    override val title = "Prüfungsamt"

    @Composable
    override fun Show() {
        PrepareFrame(
            ExaminationAuthorityScreen.title,
            DrawerAccessState.ACCESSIBLE,
        )

        Column {
            Text(text = ExaminationAuthorityScreen.title)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun Preview() {
    ExaminationAuthorityScreen.Preview()
}
