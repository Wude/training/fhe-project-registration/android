package de.fhe.proreg.ui.core.input.validators

class EmailLocalValidator(
    private val errorText: String = "Lokalteil der E-Mail-Adresse ungültig",
): Validator<String>() {
    override fun validate(value: String): String? =
        if (value.isBlank() || value.matches(emailRegex)) null else errorText

    companion object {
        private val emailRegex = "[a-zA-Z0-9._-]+".toRegex()
    }
}
