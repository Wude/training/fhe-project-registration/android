package de.fhe.proreg.ui.core

import android.os.Bundle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavHostController
import de.fhe.proreg.di.koinBaseDoubleModule
import de.fhe.proreg.di.koinViewModelModule
import org.koin.core.context.startKoin
import timber.log.Timber

abstract class ScreenWithArguments(
    icon: ImageVector,
    route: String,
    vararg arguments: NamedNavArgument,
) : Screen(icon, route, *arguments) {

    @Composable
    abstract fun Show(arguments: Bundle)

    @Composable
    abstract fun ShowPreview(arguments: Bundle)

    fun destinationWithArguments(vararg arguments: Any?) =
        destinationTemplate.format(*arguments)
}
