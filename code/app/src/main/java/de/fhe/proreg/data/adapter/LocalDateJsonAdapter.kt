@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.adapter

import de.fhe.proreg.util.format
import de.fhe.proreg.util.parseInstant
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import de.fhe.proreg.util.parseLocalDate
import java.time.Instant
import java.time.LocalDate

class LocalDateJsonAdapter {

    @FromJson
    fun fromJson(json: String): LocalDate = json.parseLocalDate()

    @ToJson
    fun toJson(value: LocalDate): String = value.format()
}
