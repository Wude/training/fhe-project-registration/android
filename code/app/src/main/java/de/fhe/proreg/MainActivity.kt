@file:OptIn(ExperimentalMaterial3Api::class)

package de.fhe.proreg

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Surface
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.ui.core.AppFrame
import de.fhe.proreg.ui.core.LocalPreferencesManager
import de.fhe.proreg.ui.core.LocalUseDarkTheme
import de.fhe.proreg.ui.theme.AppTheme
import org.koin.androidx.compose.get
import timber.log.Timber

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("MainActivity.onCreate")

        setContent {
            val prefs = get<PreferencesManager>()
            val useDarkTheme = remember { mutableStateOf(prefs.useDarkTheme) }

            //val useDarkTheme = remember { prefs.useDarkTheme }
            //prefs.useDarkTheme = true

            CompositionLocalProvider(
                LocalPreferencesManager provides prefs,
                LocalUseDarkTheme provides useDarkTheme,
            ) {
                AppTheme(useDarkTheme = useDarkTheme.value) {
                    Surface(modifier = Modifier.fillMaxSize()) {
                        AppFrame()
                    }
                }
            }
        }
    }
}
