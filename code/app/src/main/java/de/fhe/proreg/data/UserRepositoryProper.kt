package de.fhe.proreg.data

import de.fhe.proreg.data.local.UserAccess
import de.fhe.proreg.data.network.UserApi
import de.fhe.proreg.data.network.fromEntity
import de.fhe.proreg.data.network.model.Credentials
import de.fhe.proreg.data.network.model.User
import de.fhe.proreg.data.network.model.UserType
import de.fhe.proreg.data.network.toEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.transform
import retrofit2.http.Path

class UserRepositoryProper(
    private val ctx: ApiCallContext,
    private val userApi: UserApi,
    private val userAccess: UserAccess,
    private val prefs: PreferencesManager,
) : UserRepository {

    override suspend fun downloadLecturers(pageIndex: Int, pageSize: Int) {
        val result = ctx.call { userApi.getLecturers(pageIndex, pageSize) }
        if (result is ApiResult.Success) {
            result.content.forEach {
                addUser(it)
            }
        }
    }

    override fun getLecturers(): Flow<List<User>> =
        userAccess.getByType(UserType.LECTURER).map {
            val result = mutableListOf<User>()
            it.forEach { entity -> result += entity.fromEntity() }
            result
        }

    override suspend fun addUser(user: User) =
        userAccess.add(user.toEntity())

    override suspend fun login(email: String, password: String): Boolean {
        val credentials = Credentials(email, password)
        prefs.credentials = credentials
        return when (val result = ctx.call { userApi.login(credentials) }) {
            is ApiResult.Success -> {
                prefs.session = result.content
                true
            }
            else -> false
        }
    }

    override suspend fun logout() {
        prefs.session = null
        ctx.call { userApi.logout() }
    }
}
