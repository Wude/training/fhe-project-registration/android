package de.fhe.proreg.usecase

import de.fhe.proreg.data.network.model.ProjectType
import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.data.network.model.StudentProjectDocument
import de.fhe.proreg.data.network.model.User
import de.fhe.proreg.data.network.model.UserType
import kotlinx.coroutines.flow.Flow
import java.math.BigDecimal
import java.util.UUID

// ---------------------------------------------------------------------------
// User

fun interface LoginUseCase {
    suspend operator fun invoke(email: String, password: String): Boolean
}

fun interface LogoutUseCase {
    suspend operator fun invoke()
}

fun interface DownloadLecturersUseCase {
    suspend operator fun invoke()
}

fun interface GetLecturersUseCase {
    operator fun invoke(): Flow<List<User>>
}

fun interface GetProjectsUseCase {
    operator fun invoke(): Flow<List<StudentProject>>
}

fun interface DownloadRelatedProjectsUseCase {
    suspend operator fun invoke()
}

fun interface GetProjectUseCase {
    operator fun invoke(projectID: UUID): Flow<StudentProject?>
}

fun interface GetProjectDocumentUseCase {
    operator fun invoke(projectDocumentID: UUID): Flow<StudentProjectDocument?>
}

// ---------------------------------------------------------------------------
// Student

fun interface CreateProjectUseCase {
    suspend operator fun invoke(
        projectType: ProjectType,
        studentID: UUID,
        studentEmail: String,
        supervisingTutor1ID: UUID,
        supervisingTutor1Email: String,
        supervisingTutor2ID: UUID?,
        supervisingTutor2Email: String?,
        title: String,
        studentSignature: String,
    ): UUID?
}

fun interface RequestProjectDeadlineExtensionUseCase {
    suspend operator fun invoke(projectID: UUID)
}

fun interface CreateProjectDocumentUseCase {
    suspend operator fun invoke(
        projectID: UUID,
        projectDocumentID: UUID?,
        title: String,
        content: ByteArray,
    ): UUID
}

// ---------------------------------------------------------------------------
// Lecturer

fun interface AcceptProjectUseCase {
    suspend operator fun invoke(projectID: UUID)
}

fun interface RejectProjectUseCase {
    suspend operator fun invoke(projectID: UUID)
}

fun interface ChangeProjectTitleUseCase {
    suspend operator fun invoke(
        projectID: UUID,
        title: String,
    )
}

fun interface GradeProjectUseCase {
    suspend operator fun invoke(
        projectID: UUID,
        grade: BigDecimal,
    )
}

// ---------------------------------------------------------------------------
// Faculty Secretariat

fun interface RegisterProjectUseCase {
    suspend operator fun invoke(projectID: UUID)
}

// ---------------------------------------------------------------------------
// Faculty Board of Examiners

fun interface AcceptProjectDeadlineExtensionUseCase {
    suspend operator fun invoke(projectID: UUID)
}

fun interface RejectProjectDeadlineExtensionUseCase {
    suspend operator fun invoke(projectID: UUID)
}

// ---------------------------------------------------------------------------
// Administrator

fun interface CreateFacultyUseCase {
    suspend operator fun invoke(
        facultyAcronym: String,
        facultyName: String,
    )
}

fun interface CreateDepartmentUseCase {
    suspend operator fun invoke(
        facultyAcronym: String,
        departmentAcronym: String,
        departmentName: String,
    )
}

fun interface CreateUserUseCase {
    suspend operator fun invoke(
        userID: UUID?,
        email: String,
        password: String,
        userType: UserType,
        admin: Boolean,
        facultyBoardOfExaminers: Boolean,
        examinationAuthority: Boolean,
        facultyAcronyms: List<String>,
        departmentAcronyms: List<String>,
    ): UUID
}

fun interface ViewUserUseCase {
    suspend operator fun invoke(userID: UUID): User
}
