@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.adapter

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import de.fhe.proreg.data.network.model.ProjectType

class ProjectTypeJsonAdapter {

    @FromJson
    fun fromJson(value: String): ProjectType? = ProjectType.tryValueOf(value)

    @ToJson
    fun toJson(value: ProjectType?): String = value?.code ?: ""
}
