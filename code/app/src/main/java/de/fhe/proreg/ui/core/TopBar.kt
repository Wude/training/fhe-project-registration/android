package de.fhe.proreg.ui.core

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DrawerState
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarColors
import androidx.compose.material3.TopAppBarState
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.platform.LocalFocusManager
import androidx.navigation.NavHostController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar(
    navController: NavHostController,
    topAppBarState: TopAppBarState,
    drawerState: DrawerState,
    drawerAccess: State<DrawerAccessState>,
    scope: CoroutineScope,
) {
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(topAppBarState)
    var isExpanded by remember { mutableStateOf(false) }
    val focusManager = LocalFocusManager.current

    CenterAlignedTopAppBar(
        scrollBehavior = scrollBehavior,
        navigationIcon = {
            if (drawerAccess.value == DrawerAccessState.ACCESSIBLE) {
                IconButton(onClick = {
                    scope.launch {
                        if (drawerState.isClosed) {
                            focusManager.clearFocus()
                            drawerState.open()
                        } else {
                            drawerState.close()
                        }
                    }
                }) {
                    Icon(Icons.Filled.Menu, "Toggle Drawer")
                }
            } else if (drawerAccess.value == DrawerAccessState.BACK) {
                IconButton(onClick = {
                    scope.launch {
                        navController.popBackStack()
                    }
                }) {
                    Icon(Icons.Filled.ArrowBack, "Back")
                }
            }
        },
        title = { Text(text = LocalTitle.current.value) },
        actions = {
            TopBarMenu(
                LocalMenuItems.current.value,
                isExpanded,
                onSetExpanded  = {
                    if (it) {
                        focusManager.clearFocus()
                    }
                    isExpanded = it
                },
            )
        },
    )
}
