package de.fhe.proreg.data.network

import de.fhe.proreg.data.network.model.Credentials
import de.fhe.proreg.data.network.model.Session
import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.data.network.model.StudentProjectDocument
import de.fhe.proreg.data.network.model.User
import java.util.UUID
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Headers
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Body

interface UserApi {
    @POST("user/login")
    @Headers(
        "Content-Type: application/json",
        "Accept: application/json",
    )
    suspend fun login(
        @Body credentials: Credentials,
    ): Response<Session>

    @POST("user/logout")
    @Headers(
        "Content-Type: application/json",
        "Accept: application/json",
    )
    suspend fun logout(): Response<Unit>

    @GET("user/lecturer/paged/{pageIndex}/{pageSize}")
    @Headers("Accept: application/json")
    suspend fun getLecturers(
        @Path("pageIndex") pageIndex: Int,
        @Path("pageSize") pageSize: Int,
    ): Response<List<User>>
}
