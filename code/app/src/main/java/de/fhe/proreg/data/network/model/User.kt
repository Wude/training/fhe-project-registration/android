package de.fhe.proreg.data.network.model

import java.util.UUID

data class UserCreation(
    val userID: UUID,
    val email: String,
    var password: String,
    var userType: UserType,
    var admin: Boolean,
    var facultyBoardOfExaminers: Boolean,
    var facultyAcronyms: List<String>,
    var departmentAcronyms: List<String>,
)

data class User(
    val userID: UUID,
    val email: String,
    var userType: UserType,
    var admin: Boolean,
    var facultyBoardOfExaminers: Boolean,
    var facultyAcronyms: List<String>?,
    var departmentAcronyms: List<String>?,
)
