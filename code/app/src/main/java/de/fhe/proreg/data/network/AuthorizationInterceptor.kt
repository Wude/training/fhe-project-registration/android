package de.fhe.proreg.data.network

import de.fhe.proreg.data.PreferencesManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.singleOrNull
import kotlinx.coroutines.runBlocking
import java.io.IOException
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.Request

class AuthorizationInterceptor(
    private val prefs: PreferencesManager,
) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var mainRequest: Request = chain.request()

        runBlocking(Dispatchers.IO) {
            if (prefs.accessToken != null) {
                mainRequest = mainRequest.setAuth()
            }
        }

        return chain.proceed(mainRequest)
    }

    private fun Request.setAuth(): Request {
        return newBuilder()
            .header(AUTH, "Bearer ${this@AuthorizationInterceptor.prefs.accessToken}")
            .build()
    }

    companion object {
        private const val AUTH = "Authorization";
    }

}
