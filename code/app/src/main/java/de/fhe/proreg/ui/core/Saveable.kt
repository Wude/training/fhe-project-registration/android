package de.fhe.proreg.ui.core

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.Saver
import androidx.compose.runtime.saveable.autoSaver
import androidx.compose.runtime.saveable.listSaver
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import kotlin.properties.PropertyDelegateProvider
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

@Suppress("UNCHECKED_CAST")
@Composable
fun <T: Screen> rememberSaveableList(
    screens: ScreenEnum,
) = rememberSaveable(saver = listSaver(
    save = { state -> state.value.map { it.route } },
    restore = { routes -> mutableStateOf(routes.map { screens.allByRoute[it]!! as T }) },
)
) {
    mutableStateOf(listOf<T>())
}

/**
 * Inter-opt between [SavedStateHandle] and [Saver] so that any state holder that is
 * being saved via [rememberSaveable] with a custom [Saver] can also be saved with
 * [SavedStateHandle].
 *
 * The key is automatically retrieved as the name of the property this delegate is being used
 * to create.
 *
 * The delegated [MutableState] should be the only way that a value is saved or restored from the
 * [SavedStateHandle] with the automatic key.
 *
 * Using the same key again with another [SavedStateHandle] method is not supported, as values
 * won't cross-set or communicate updates.
 *
 * Use this overload to allow delegating to a mutable state just like you can with
 * `rememberSaveable`:
 * ```
 * var value by savedStateHandle.saveable { mutableStateOf("initialValue") }
 * ```
 *
 * @sample androidx.lifecycle.viewmodel.compose.samples.SnapshotStateViewModelWithDelegates
 */
@SavedStateHandleSaveableApi
@JvmName("saveableNullableMutableState")
fun <T : Any, M : MutableState<T?>> SavedStateHandle.saveableNullable(
    stateSaver: Saver<T?, out Any> = autoSaver(),
    init: () -> M,
): PropertyDelegateProvider<Any?, ReadWriteProperty<Any?, T?>> =
    PropertyDelegateProvider<Any?, ReadWriteProperty<Any?, T?>> { _, property ->
        val mutableState = saveable(
            key = property.name,
            stateSaver = stateSaver,
            init = init
        )

        // Create a property that delegates to the mutableState
        object : ReadWriteProperty<Any?, T?> {
            override fun getValue(thisRef: Any?, property: KProperty<*>): T? =
                mutableState.getValue(thisRef, property)

            override fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) =
                mutableState.setValue(thisRef, property, value)
        }
    }

/**
 * Inter-opt between [SavedStateHandle] and [Saver] so that any state holder that is
 * being saved via [rememberSaveable] with a custom [Saver] can also be saved with
 * [SavedStateHandle].
 *
 * The key is automatically retrieved as the name of the property this delegate is being used
 * to create.
 *
 * The delegated [MutableState] should be the only way that a value is saved or restored from the
 * [SavedStateHandle] with the automatic key.
 *
 * Using the same key again with another [SavedStateHandle] method is not supported, as values
 * won't cross-set or communicate updates.
 *
 * Use this overload to allow delegating to a mutable state just like you can with
 * `rememberSaveable`:
 * ```
 * var value by savedStateHandle.saveable { mutableStateOf("initialValue") }
 * ```
 *
 * @sample androidx.lifecycle.viewmodel.compose.samples.SnapshotStateViewModelWithDelegates
 */
@Suppress("UNCHECKED_CAST")
@SavedStateHandleSaveableApi
@JvmName("saveableNullableMutableState")
fun <T : Any, M : MutableState<T?>> SavedStateHandle.saveableNullableState(
    propertyName: String,
    stateSaver: Saver<T?, out Any> = autoSaver(),
    init: () -> M,
): M = saveable(
        key = propertyName,
        stateSaver = stateSaver,
        init = init
    ) as M
