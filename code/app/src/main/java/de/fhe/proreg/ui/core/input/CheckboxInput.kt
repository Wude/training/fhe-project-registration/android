package de.fhe.proreg.ui.core.input

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Checkbox
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import de.fhe.proreg.ui.core.input.validators.Validator

class CheckboxInput(
    label: String,
    checked: Boolean,
    onCheckedChange: (value: Boolean) -> Unit,
    vararg validators: Validator<Boolean>,
): Input<Boolean>(
    label = label,
    value = checked,
    onValueChange = onCheckedChange,
    validators = validators,
) {
    @Composable
    fun show(
        modifier: Modifier = Modifier,
    ): CheckboxInput {
        errorTextLines = remember { mutableStateOf(listOf<String>()) }

        Column(modifier = Modifier.padding(top = 8.dp)) {
            Column(modifier = modifier
                .fillMaxWidth()
                .clip(MaterialTheme.shapes.small)
                .clickable(
                    indication = rememberRipple(color = MaterialTheme.colorScheme.primary),
                    interactionSource = remember { MutableInteractionSource() },
                    onClick = { onValueChange(!value) }
                )
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.padding(vertical = 8.dp)
                ) {
                    Checkbox(
                        checked = value,
                        onCheckedChange = null
                    )

                    Spacer(Modifier.size(6.dp))

                    Text(
                        text = label,
                        style = MaterialTheme.typography.labelMedium,
                    )
                }
            }

            if (hasError) {
                Text(
                    text = errorTextLines.value.joinToString("\n"),
                    modifier = Modifier.padding(
                        horizontal = 8.dp,
                        vertical = 4.dp
                    ),
                    color = MaterialTheme.colorScheme.error,
                )
            }
        }
        return this
    }
}
