@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.adapter

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import de.fhe.proreg.data.network.model.ProjectStatus

class ProjectStatusJsonAdapter {

    @FromJson
    fun fromJson(value: String): ProjectStatus? = ProjectStatus.tryValueOf(value)

    @ToJson
    fun toJson(value: ProjectStatus?): String = value?.code ?: ""
}
