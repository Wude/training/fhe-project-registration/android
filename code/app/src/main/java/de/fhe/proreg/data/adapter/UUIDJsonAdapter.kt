@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.adapter

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.util.UUID

class UUIDJsonAdapter {

    @FromJson
    fun fromJson(json: String): UUID? = UUID.fromString(json)

    @ToJson
    fun toJson(value: UUID?): String = value?.toString() ?: ""
}
