package de.fhe.proreg.data.local

import androidx.room.Dao
import androidx.room.Database
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.OnConflictStrategy
import de.fhe.proreg.data.adapter.UUIDJsonAdapter
import de.fhe.proreg.data.local.model.StudentProjectDocumentEntity
import kotlinx.coroutines.flow.Flow
import java.util.UUID

@Dao
interface StudentProjectDocumentAccess {
    @Query("select * from StudentProjectDocumentEntity")
    fun getAll(): Flow<List<StudentProjectDocumentEntity>>

    @Query("select * from StudentProjectDocumentEntity where projectId = :projectId")
    fun getAll(projectId: UUID): Flow<List<StudentProjectDocumentEntity>>

    @Query("select * from StudentProjectDocumentEntity where projectDocumentId = :projectDocumentId")
    fun get(projectDocumentId: UUID): Flow<StudentProjectDocumentEntity?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(project: StudentProjectDocumentEntity)

    @Delete
    suspend fun remove(vararg documents: StudentProjectDocumentEntity)

    @Query("delete from StudentProjectDocumentEntity")
    suspend fun removeAll()
}
