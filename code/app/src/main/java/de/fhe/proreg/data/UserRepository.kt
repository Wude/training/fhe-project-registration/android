package de.fhe.proreg.data

import de.fhe.proreg.data.network.model.User
import kotlinx.coroutines.flow.Flow

interface UserRepository {

    suspend fun downloadLecturers(pageIndex: Int = 0, pageSize: Int = 20)

    fun getLecturers(): Flow<List<User>>

    suspend fun addUser(user: User)

    suspend fun login(email: String, password: String): Boolean

    suspend fun logout()
}
