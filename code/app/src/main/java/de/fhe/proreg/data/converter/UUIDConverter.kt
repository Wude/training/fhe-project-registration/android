@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.converter

import androidx.room.TypeConverter
import java.nio.ByteBuffer
import java.util.UUID

class UUIDConverter {

    @TypeConverter
    fun toUUID(value: ByteArray?): UUID? {
        if (value != null) {
            val buffer = ByteBuffer.wrap(value)
            val firstLong = buffer.long
            val secondLong = buffer.long
            return UUID(firstLong, secondLong)
        }
        return null
    }

    @TypeConverter
    fun fromUUID(value: UUID?): ByteArray? {
        if (value != null) {
            val bytes = ByteArray(16)
            val buffer = ByteBuffer.wrap(bytes)
            buffer.putLong(value.mostSignificantBits)
            buffer.putLong(value.leastSignificantBits)
            return buffer.array()
        }
        return null
    }
}
