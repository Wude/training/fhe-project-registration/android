@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.converter

import androidx.room.TypeConverter
import de.fhe.proreg.data.network.model.ProjectStatus

class ProjectStatusConverter {

    @TypeConverter
    fun toProjectStatus(value: String?): ProjectStatus? = ProjectStatus.tryValueOf(value)

    @TypeConverter
    fun fromProjectStatus(value: ProjectStatus?): String? = value?.code
}
