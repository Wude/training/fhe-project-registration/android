package de.fhe.proreg.ui.core

enum class DrawerAccessState {
    ACCESSIBLE,
    BACK,
    HIDDEN,
}
