package de.fhe.proreg.ui.screen.student

import android.os.Bundle
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.joelkanyi.composesignature.ComposeSignature
import de.fhe.proreg.data.ProjectRepository
import de.fhe.proreg.data.network.model.ProjectStatus
import de.fhe.proreg.data.network.model.ProjectType
import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.data.network.model.User
import de.fhe.proreg.ui.core.DrawerAccessState
import de.fhe.proreg.ui.core.FloatingActionButton
import de.fhe.proreg.ui.core.LocalBottomShortcuts
import de.fhe.proreg.ui.core.LocalNavController
import de.fhe.proreg.ui.core.LocalSnackbarState
import de.fhe.proreg.ui.core.PrepareFrame
import de.fhe.proreg.ui.core.Preview
import de.fhe.proreg.ui.core.ScreenWithArguments
import de.fhe.proreg.ui.core.ScreenWithoutArguments
import de.fhe.proreg.ui.core.TextDisplay
import de.fhe.proreg.ui.core.TopBarMenuItem
import de.fhe.proreg.ui.core.collectValue
import de.fhe.proreg.ui.core.input.CheckboxInput
import de.fhe.proreg.ui.core.input.Input
import de.fhe.proreg.ui.core.input.SingleSelectInput
import de.fhe.proreg.ui.core.input.TextInput
import de.fhe.proreg.ui.core.input.areValid
import de.fhe.proreg.ui.core.input.validators.NotNullValidator
import de.fhe.proreg.ui.core.input.validators.TextHasContentValidator
import de.fhe.proreg.ui.core.input.validators.TrueValidator
import de.fhe.proreg.ui.core.navigateUpTo
import de.fhe.proreg.ui.screen.Screens
import de.fhe.proreg.ui.screen.projectStatusDescription
import de.fhe.proreg.ui.screen.projectTypeDescription
import de.fhe.proreg.util.TimeUtils
import de.fhe.proreg.util.toGermanDateTime
import de.fhe.proreg.util.toLongText
import de.fhe.proreg.util.toMediumText
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.koin.androidx.compose.koinViewModel
import org.koin.compose.koinInject
import java.util.UUID

object StudentCreateProjectScreen: ScreenWithoutArguments(
    icon = Icons.Filled.Person,
    route = "student/project/create",
) {
    override val title = "Projekterstellung"

    @Composable
    override fun Show() {
        PrepareFrame(
            title,
            DrawerAccessState.BACK,
        )
        val vm: StudentCreateProjectViewModel = koinViewModel()
        val lecturers = remember(vm) { vm.getLecturers() }.collectAsStateWithLifecycle(listOf())
        //val lecturers = remember(vm) { vm.lecturers }.collectAsStateWithLifecycle(listOf())
        //vm.downloadLecturers()

        val scope = rememberCoroutineScope()
        val createClear = remember { mutableStateOf(true) }
        val navController = LocalNavController.current
        val snackbar = LocalSnackbarState.current

        // Box(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier
                .padding(12.dp)
                .fillMaxSize(),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Column(
                modifier = Modifier
                    .verticalScroll(rememberScrollState())
                    .padding(12.dp)
                    .fillMaxSize(),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                val inputs = mutableListOf<Input<*>>()
                val title = TextInput(
                    label = "Titel",
                    value = vm.title,
                    onValueChange = { vm.updateTitle(it) },
                    TextHasContentValidator(),
                ).show()
                inputs += title

                val projectTypes = remember { mutableStateOf(ProjectType.values().toList()) }
                val projectType = SingleSelectInput(
                    label = "Projektart",
                    value = vm.projectType,
                    onValueChange = { vm.updateProjectType(it) },
                    onDisplayValue = { projectTypeDescription(it) },
                    values = projectTypes,
                    onSearch = { _ -> },
                    NotNullValidator(),
                ).show()
                inputs += projectType

                val supervisingTutor1 = SingleSelectInput(
                    label = when (vm.projectType) {
                        ProjectType.BACHELORS_THESIS -> "Erstgutachter"
                        ProjectType.MASTERS_THESIS -> "Erstgutachter"
                        else -> "Gutachter"
                    },
                    value = vm.supervisingTutor1.value,
                    onValueChange = { vm.updateSupervisingTutor1(it) },
                    onDisplayValue = { it.email },
                    values = lecturers,
                    onSearch = { _ -> },
                    NotNullValidator(),
                ).show()
                inputs += supervisingTutor1

                if (vm.projectType == ProjectType.BACHELORS_THESIS ||
                    vm.projectType == ProjectType.MASTERS_THESIS
                ) {
                    val supervisingTutor2 = SingleSelectInput(
                        label = "Zweitgutachter",
                        value = vm.supervisingTutor2.value,
                        onValueChange = { vm.updateSupervisingTutor2(it) },
                        onDisplayValue = { it.email },
                        values = lecturers,
                        onSearch = { _ -> },
                        NotNullValidator(),
                    ).show(
                        enabled = vm.projectType == ProjectType.BACHELORS_THESIS ||
                                vm.projectType == ProjectType.MASTERS_THESIS,
                    )
                    inputs += supervisingTutor2
                }

                var signatureVisible by remember { mutableStateOf(false) }

                val signature = CheckboxInput(
                    label = "Signatur/Unterschrift",
                    checked = vm.signatureBitmap.value != null,
                    onCheckedChange = {
                        signatureVisible = true
                    },
                    TrueValidator(),
                ).show()
                inputs += signature

                if (signatureVisible) {
                    val focusManager = LocalFocusManager.current

                    val onDismissRequest: () -> Unit = {
                        signatureVisible = false
                        focusManager.clearFocus()
                    }

                    Dialog(onDismissRequest = onDismissRequest) {
                        Surface(
                            modifier = Modifier.fillMaxWidth(),
                            shape = RoundedCornerShape(10.dp),
                        ) {
                            ComposeSignature(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(16.dp),
                                signaturePadColor = MaterialTheme.colorScheme.surface,
                                signatureColor = MaterialTheme.colorScheme.onSurface,
                                signaturePadHeight = 400.dp,
                                signatureThickness = 5f,
                                onComplete = {
                                    vm.updateSignatureBitmap(it.asImageBitmap())
                                    onDismissRequest()
                                },
                                onClear = { vm.updateSignatureBitmap(null) }
                            )
                        }
                    }
                }

                Button(
                    modifier = Modifier
                        .padding(8.dp)
                        .align(alignment = Alignment.End)
                        .fillMaxWidth(),
                    enabled = createClear.value && inputs.areValid(),
                    onClick = {
                        scope.launch {
                            createClear.value = false
                            if (vm.createProject()) {
                                navController.navigateUpTo(Screens.Student.destination())
                                snackbar.showSnackbar(
                                    message  = "Projekterstellung erfolgreich",
                                    duration = SnackbarDuration.Short,
                                )
                            } else {
                                snackbar.showSnackbar(
                                    message  = "Projekterstellung nicht erfolgreich",
                                    duration = SnackbarDuration.Short,
                                )
                            }
                            createClear.value = true
                        }
                    }
                ) {
                    Text(text = "Erstellen")
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewCreateProject() {
    StudentCreateProjectScreen.Preview()
}
