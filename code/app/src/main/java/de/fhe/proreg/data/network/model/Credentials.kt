package de.fhe.proreg.data.network.model

data class Credentials(
    val email: String,
    val password: String)
