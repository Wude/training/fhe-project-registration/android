package de.fhe.proreg.usecase

import java.math.BigDecimal
import java.util.UUID

class AcceptProject() {
    operator fun invoke(projectID: UUID) {
    }
}

class RejectProject() {
    operator fun invoke(projectID: UUID) {
    }
}

class ChangeProjectTitle() {
    operator fun invoke(
        projectID: UUID,
        title: String,
    ) {
    }
}

class GradeProject() {
    operator fun invoke(
        projectID: UUID,
        grade: BigDecimal,
    ) {
    }
}
