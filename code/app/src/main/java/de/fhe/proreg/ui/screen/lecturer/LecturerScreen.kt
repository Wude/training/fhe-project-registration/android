package de.fhe.proreg.ui.screen.lecturer

import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import de.fhe.proreg.ui.core.DrawerAccessState
import de.fhe.proreg.ui.core.PrepareFrame
import de.fhe.proreg.ui.core.Preview
import de.fhe.proreg.ui.core.ScreenWithoutArguments

object LecturerScreen: ScreenWithoutArguments(
    icon = Icons.Filled.Person,
    route = "lecturer",
    displayRank = 2,
) {
    override val title = "Lehrende"

    @Composable
    override fun Show() {
        PrepareFrame(
            LecturerScreen.title,
            DrawerAccessState.ACCESSIBLE,
        )

        Column {
            Text(text = LecturerScreen.title)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun Preview() {
    LecturerScreen.Preview()
}
