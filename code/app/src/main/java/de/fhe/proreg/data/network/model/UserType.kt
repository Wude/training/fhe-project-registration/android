package de.fhe.proreg.data.network.model

/**
* The user types.
*/
enum class UserType(val code: String) {

    /**
    * A student.
    */
    STUDENT(UserGroup.STUDENT),

    /**
    * A lecturer.
    */
    LECTURER(UserGroup.LECTURER),

    /**
    * A staff member of the faculty secreteriat.
    */
    FACULTY_SECRETARIAT_STAFF(UserGroup.FACULTY_SECRETARIAT_STAFF),

    /**
    * A staff member of the examination authority.
    */
    EXAMINATION_AUTHORITY_STAFF(UserGroup.EXAMINATION_AUTHORITY_STAFF),

    /**
    * An administration staff member.
    */
    ADMINISTRATOR_STAFF(UserGroup.ADMINISTRATOR);

    companion object {

        val valuesByCode = HashMap<String, UserType>()

        init {
            val userTypeValues = UserType.values()

            for (userType in userTypeValues) {
                valuesByCode.put(userType.code, userType);
                valuesByCode.put(userType.name, userType);
            }
        }

        fun tryValueOf(idOrCode: String?, defaultValue: UserType): UserType = valuesByCode.get(idOrCode) ?: defaultValue
        fun tryValueOf(idOrCode: String?): UserType? = valuesByCode.get(idOrCode)
    }
}
