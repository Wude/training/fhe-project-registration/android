package de.fhe.proreg

import android.app.Application
import de.fhe.proreg.di.koinBaseModule
import de.fhe.proreg.di.koinDataModule
import de.fhe.proreg.di.koinUseCaseModule
import de.fhe.proreg.di.koinNetworkModule
import de.fhe.proreg.di.koinRoomModule
import de.fhe.proreg.di.koinViewModelModule
//import de.fhe.proreg.domain.Repository
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            // androidLogger(if (BuildConfig.DEBUG) Level.DEBUG else Level.NONE)
            androidContext(this@App)
            ///*
            modules(koinBaseModule)
            modules(koinRoomModule)
            modules(koinDataModule)
            modules(koinNetworkModule)
            modules(koinUseCaseModule)
            modules(koinViewModelModule)
            //*/
        }
    }

    companion object {
        @JvmStatic private val TAG = App::class.java.simpleName

        @Suppress("ConstantConditionIf")
        @JvmStatic val isDebugLocal = BuildConfig.BUILD_TYPE == "debugLocal"
    }
}
