package de.fhe.proreg.ui.screen

import de.fhe.proreg.data.network.model.ProjectStatus
import de.fhe.proreg.data.network.model.ProjectType

fun projectTypeDescription(projectType: ProjectType) = when(projectType) {
        ProjectType.BACHELORS_PROJECT -> "BA-Projekt"
        ProjectType.BACHELORS_THESIS -> "BA-Arbeit"
        ProjectType.MASTERS_PROJECT -> "MA-Projekt"
        ProjectType.MASTERS_THESIS -> "MA-Arbeit"
}

fun projectStatusDescription(status: ProjectStatus) = when(status) {
        ProjectStatus.SUBMITTED -> "Eingereicht"
        ProjectStatus.REJECTED -> "Abgelehnt"
        ProjectStatus.ACCEPTED -> "Angenommen"
        ProjectStatus.REGISTERED -> "Registriert"
        ProjectStatus.DEADLINE_EXTENSION_REQUESTED -> "Fristverlängerung beantragt"
        ProjectStatus.DEADLINE_EXTENSION_REJECTED -> "Fristverlängerung abgelehnt"
        ProjectStatus.DEADLINE_EXTENSION_GRANTED -> "Fristverlängerung bewilligt"
        ProjectStatus.GRADED -> "Benotet"
        ProjectStatus.COMPLETED -> "Abgeschlossen"
    }
