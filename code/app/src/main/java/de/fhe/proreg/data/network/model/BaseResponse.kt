package de.fhe.proreg.data.network.model

data class BaseResponse(
    var status: Int,
    var reason: String
)
