package de.fhe.proreg.data.local

import androidx.room.Dao
import androidx.room.Database
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.OnConflictStrategy
import de.fhe.proreg.data.adapter.UUIDJsonAdapter
import de.fhe.proreg.data.local.model.StudentProjectEntity
import kotlinx.coroutines.flow.Flow
import java.util.UUID

@Dao
interface StudentProjectAccess {
    @Query("select * from StudentProjectEntity")
    fun getAll(): Flow<List<StudentProjectEntity>>

    @Query("select * from StudentProjectEntity where projectID = :projectID")
    fun get(projectID: UUID?): Flow<StudentProjectEntity?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun add(project: StudentProjectEntity)

    @Delete
    suspend fun remove(vararg projects: StudentProjectEntity)

    @Query("delete from StudentProjectEntity")
    suspend fun removeAll(): Unit
}
