package de.fhe.proreg.ui.screen.user

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Login
import androidx.compose.material3.Button
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.ui.core.DrawerAccessState
import de.fhe.proreg.ui.core.LocalNavController
import de.fhe.proreg.ui.core.LocalSession
import de.fhe.proreg.ui.core.LocalSnackbarState
import de.fhe.proreg.ui.core.PrepareFrame
import de.fhe.proreg.ui.core.Preview
import de.fhe.proreg.ui.core.ScreenWithoutArguments
import de.fhe.proreg.ui.core.TopBarMenuItem
import de.fhe.proreg.ui.core.input.EmailInput
import de.fhe.proreg.ui.core.input.PasswordInput
import de.fhe.proreg.ui.core.input.validators.EmailValidator
import de.fhe.proreg.ui.core.input.validators.TextHasContentValidator
import de.fhe.proreg.ui.core.input.areValid
import de.fhe.proreg.ui.screen.Screens
import kotlinx.coroutines.launch
import org.koin.androidx.compose.koinViewModel
import org.koin.compose.koinInject

object LoginScreen: ScreenWithoutArguments(
    icon = Icons.Filled.Login,
    route = "login",
    displayRank = 0,
) {
    override val title = "Anmeldung"

    @Composable
    override fun Show() {
        val vm: LoginViewModel = koinViewModel()
        val scope = rememberCoroutineScope()
        val loginClear = remember { mutableStateOf(true) }
        val navController = LocalNavController.current
        val snackbar = LocalSnackbarState.current
        val session = LocalSession.current
        // val prefs = koinInject<PreferencesManager>()

        PrepareFrame(
            "FHE-Projektregistrierung",
            DrawerAccessState.ACCESSIBLE,
        )

        Column(
            modifier = Modifier
                .padding(12.dp)
                .fillMaxSize(),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(Modifier.height(60.dp))

            val email = EmailInput(
                label = "E-Mail",
                // label = "E-Mail (@fh-erfurt.de)",
                value = vm.email,
                onValueChange = { vm.updateEmail(it) },
                TextHasContentValidator(),
                EmailValidator(),
                // EmailLocalValidator(),
            ).show()
            val password = PasswordInput(
                label = "Passwort",
                value = vm.password,
                onValueChange = { vm.updatePassword(it) },
                TextHasContentValidator(),
            ).show()
            val inputs = listOf(email, password)

            Button(
                modifier = Modifier
                    .padding(8.dp)
                    .align(alignment = Alignment.End)
                    .fillMaxWidth(),
                enabled = loginClear.value && inputs.areValid(),
                onClick = {
                    scope.launch {
                        loginClear.value = false
                        if (vm.login()) {
                            navController.navigate(Screens.Start.destination())
                            snackbar.showSnackbar(
                                // message  = "Anmeldung erfolgreich",
                                message  = "Anmeldung erfolgreich (${session.value?.user?.userType})",
                                duration = SnackbarDuration.Short,
                            )
                        } else {
                            snackbar.showSnackbar(
                                message  = "Anmeldung nicht erfolgreich",
                                duration = SnackbarDuration.Short,
                            )
                        }
                        loginClear.value = true
                    }
                }
            ) {
                Text(text = "Login")
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewLogin() {
    LoginScreen.Preview()
}
