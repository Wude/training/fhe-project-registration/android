package de.fhe.proreg.usecase

import de.fhe.proreg.data.network.model.User
import de.fhe.proreg.data.network.model.UserType
import java.util.UUID

class CreateFaculty() {
    operator fun invoke(
        facultyAcronym: String,
        facultyName: String,
    ) {
    }
}

class CreateDepartment() {
    operator fun invoke(
        facultyAcronym: String,
        departmentAcronym: String,
        departmentName: String,
    ) {
    }
}

class CreateUser() {
    operator fun invoke(
        userID: UUID?,
        email: String,
        password: String,
        userType: UserType,
        admin: Boolean,
        facultyBoardOfExaminers: Boolean,
        examinationAuthority: Boolean,
        facultyAcronyms: List<String>,
        departmentAcronyms: List<String>,
    ): UUID {
        val uID = userID ?: UUID.randomUUID()
        return uID
    }
}

class ViewUser() {
    operator fun invoke(
        userID: UUID,
    ): User {
        val user = null
        return user!!
    }
}
