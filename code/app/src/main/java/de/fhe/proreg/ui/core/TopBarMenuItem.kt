package de.fhe.proreg.ui.core

import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.runtime.saveable.Saver
import androidx.compose.runtime.saveable.listSaver
import timber.log.Timber

class TopBarMenuItem private constructor(
    var icon: ImageVector?,
    var isInOverflow: Boolean,
    var title: String,
    var action: () -> Unit,
) {
    constructor(
        title: String,
        action: () -> Unit,
    ) : this(null, true, title, action)

    constructor(
        icon: ImageVector,
        title: String,
        action: () -> Unit,
        isInOverflow: Boolean = false,
    ) : this(icon, isInOverflow, title, action)
}
