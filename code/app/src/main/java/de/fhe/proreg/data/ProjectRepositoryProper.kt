package de.fhe.proreg.data

import de.fhe.proreg.data.local.StudentProjectAccess
import de.fhe.proreg.data.local.StudentProjectDocumentAccess
import de.fhe.proreg.data.network.ProjectApi
import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.data.network.toEntity
import de.fhe.proreg.data.network.fromEntity
import de.fhe.proreg.data.network.model.StudentProjectCreationRequest
import de.fhe.proreg.data.network.model.StudentProjectDocument
//import de.fhe.proreg.domain.Repository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.util.UUID

class ProjectRepositoryProper(
    private val ctx: ApiCallContext,
    private val projectApi: ProjectApi,
    private val projectAccess: StudentProjectAccess,
    private val projectDocumentAccess: StudentProjectDocumentAccess,
) : ProjectRepository {

    override suspend fun downloadRelatedProjects(pageIndex: Int, pageSize: Int) {
        val result = ctx.call { projectApi.getRelatedProjects(pageIndex, pageSize) }
        if (result is ApiResult.Success) {
            result.content.forEach {
                addProject(it)
            }
        }
    }

    override suspend fun uploadProject(project: StudentProjectCreationRequest): UUID? {
        val result = ctx.call { projectApi.addProject(project) }
        return if (result is ApiResult.Success) {
            result.content.projectID
        } else {
            null
        }
    }

    override fun getProjects(): Flow<List<StudentProject>> =
        projectAccess.getAll().map {
            val result = mutableListOf<StudentProject>()
            it.forEach { entity -> result += entity.fromEntity() }
            result
        }

    override fun getProject(projectID: UUID?): Flow<StudentProject?> =
        projectAccess.get(projectID).map { it?.fromEntity() }

    override suspend fun addProject(project: StudentProject) =
        projectAccess.add(project.toEntity())

    override fun getProjectDocument(projectDocumentID: UUID): Flow<StudentProjectDocument?> =
        projectDocumentAccess.get(projectDocumentID).map { it?.fromEntity() }

    override suspend fun addProjectDocument(projectDocument: StudentProjectDocument) =
        projectDocumentAccess.add(projectDocument.toEntity())
}
