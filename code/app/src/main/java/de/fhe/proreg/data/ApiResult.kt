package de.fhe.proreg.data

/**
* Based on:
* https://proandroiddev.com/why-you-need-use-cases-interactors-142e8a6fe576
* https://proandroiddev.com/jwt-authentication-and-refresh-token-in-android-with-retrofit-interceptor-authenticator-da021f7f7534
*/
sealed class ApiResult<out T> {
    object Loading: ApiResult<Nothing>()
    data class Success<out T>(val content: T) : ApiResult<T>()
    data class Failure(
        val status: Int,
        val reason: String,
    ): ApiResult<Nothing>()
}
