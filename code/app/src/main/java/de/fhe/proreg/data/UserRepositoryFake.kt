package de.fhe.proreg.data

import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.data.network.model.User
import de.fhe.proreg.data.network.model.UserType
import de.fhe.proreg.data.network.toEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.util.UUID

class UserRepositoryFake : UserRepository {
    private val usersById = mutableMapOf<UUID, User>()

    override suspend fun downloadLecturers(pageIndex: Int, pageSize: Int) { }

    override fun getLecturers(): Flow<List<User>> = flow {
        val result = mutableListOf<User>()
        usersById.values.filter { it.userType == UserType.LECTURER }.forEach { result += it }
        emit(result)
    }

    override suspend fun addUser(user: User) {
        usersById[user.userID] = user
    }

    override suspend fun login(email: String, password: String): Boolean = false

    override suspend fun logout() { }
}
