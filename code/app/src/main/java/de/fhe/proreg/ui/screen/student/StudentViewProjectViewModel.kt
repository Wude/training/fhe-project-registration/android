package de.fhe.proreg.ui.screen.student

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import de.fhe.proreg.usecase.GetProjectUseCase
import java.util.UUID

@OptIn(SavedStateHandleSaveableApi::class)
class StudentViewProjectViewModel(
    savedStateHandle: SavedStateHandle,
    private val getProjectUseCase: GetProjectUseCase,
): ViewModel() {

    fun getProject(projectID: UUID) = getProjectUseCase(projectID)
}
