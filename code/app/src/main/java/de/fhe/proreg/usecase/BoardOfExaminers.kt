package de.fhe.proreg.usecase

import java.util.UUID

class AcceptProjectDeadlineExtension() {
    operator fun invoke(projectID: UUID) {
    }
}

class RejectProjectDeadlineExtension() {
    operator fun invoke(projectID: UUID) {
    }
}
