package de.fhe.proreg.ui.screen.user

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DarkMode
import androidx.compose.material.icons.filled.LightMode
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Button
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.ui.core.DrawerAccessState
import de.fhe.proreg.ui.core.LocalNavController
import de.fhe.proreg.ui.core.LocalSession
import de.fhe.proreg.ui.core.LocalSnackbarState
import de.fhe.proreg.ui.core.LocalUseDarkTheme
import de.fhe.proreg.ui.core.PrepareFrame
import de.fhe.proreg.ui.core.Preview
import de.fhe.proreg.ui.core.Screen
import de.fhe.proreg.ui.core.ScreenWithoutArguments
import de.fhe.proreg.ui.core.TopBarMenuItem
import de.fhe.proreg.ui.core.input.SwitchInput
import de.fhe.proreg.ui.core.input.SingleSelectInput
import de.fhe.proreg.ui.core.input.validators.NotNullValidator
import de.fhe.proreg.ui.core.input.validators.TextHasContentValidator
import de.fhe.proreg.ui.screen.Screens
import kotlinx.coroutines.launch
import org.koin.androidx.compose.koinViewModel
import org.koin.compose.koinInject

object SettingsScreen: ScreenWithoutArguments(
    icon = Icons.Filled.Settings,
    route = "settings",
    displayRank = 6,
) {
    override val title = "Einstellungen"

    @Composable
    override fun Show() {
        val vm: SettingsViewModel = koinViewModel()
        val scope = rememberCoroutineScope()
        val logoutClear = remember { mutableStateOf(true) }
        val navController = LocalNavController.current
        val session = LocalSession.current
        val useDarkTheme = LocalUseDarkTheme.current
        val prefs = koinInject<PreferencesManager>()

        PrepareFrame(
            SettingsScreen.title,
            DrawerAccessState.ACCESSIBLE,
        )

        Column(
            modifier = Modifier
                .padding(12.dp)
                .fillMaxSize(),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            SwitchInput(
                label = "Helle/Dunkle Darstellung",
                checked = useDarkTheme.value,
                onCheckedChange = {
                    useDarkTheme.value = it
                    prefs.useDarkTheme = it
                },
            ).show(
                trueIcon = Icons.Filled.DarkMode,
                falseIcon = Icons.Filled.LightMode,
            )

            Button(
                modifier = Modifier
                    .padding(8.dp)
                    .align(alignment = Alignment.End)
                    .fillMaxWidth(),
                enabled = logoutClear.value && session.value != null,
                // enabled = logoutClear.value && prefs.accessToken != null,
                onClick = { scope.launch {
                    logoutClear.value = false
                    vm.logout()
                    navController.navigate(Screens.Start.destination())
                    logoutClear.value = true
                } }
            ) {
                Text(text = "Logout")
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewSettings() {
    SettingsScreen.Preview()
}
