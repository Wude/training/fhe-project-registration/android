@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.adapter

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import de.fhe.proreg.data.network.model.UserType

class UserTypeJsonAdapter {

    @FromJson
    fun fromJson(value: String): UserType? = UserType.tryValueOf(value)

    @ToJson
    fun toJson(value: UserType?): String = value?.code ?: ""
}
