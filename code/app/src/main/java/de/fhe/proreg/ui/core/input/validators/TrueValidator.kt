package de.fhe.proreg.ui.core.input.validators

class TrueValidator(
    private val errorText: String = "Bestätigung erforderlich",
): Validator<Boolean>() {
    override fun validate(value: Boolean): String? =
        if (value) null else errorText
}
