package de.fhe.proreg.ui.core

import android.os.Bundle
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.rememberDrawerState
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.listSaver
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.data.ProjectRepository
import de.fhe.proreg.data.network.model.ProjectType
import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.di.PreviewData
import de.fhe.proreg.di.koinBaseDoubleModule
import de.fhe.proreg.di.koinDataModule
import de.fhe.proreg.di.koinDataDoubleModule
import de.fhe.proreg.di.koinUseCaseModule
import de.fhe.proreg.di.koinViewModelModule
import de.fhe.proreg.di.koinViewModelDoubleModule
import de.fhe.proreg.ui.core.LocalBottomShortcuts
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.runBlocking
import org.koin.compose.koinInject
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.startKoin
import timber.log.Timber
import java.util.UUID

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun Screen.PreparePreview(content: @Composable () -> Unit) {
    startKoin {
        modules(koinBaseDoubleModule)
        modules(koinDataDoubleModule)
        modules(koinUseCaseModule)
        modules(koinViewModelDoubleModule)
    }
    PreviewData().prepare()

    val prefs = koinInject<PreferencesManager>()
    val scope = rememberCoroutineScope()
    val useDarkTheme = remember { mutableStateOf(false) }
    val navController = rememberNavController()
    val topAppBarState = rememberTopAppBarState()
    val snackbarState = remember { SnackbarHostState() }
    val drawerState = rememberDrawerState(DrawerValue.Closed)
    val title = remember { mutableStateOf(title) }
    val drawerAccess = remember { mutableStateOf(DrawerAccessState.ACCESSIBLE) }
    val drawerShortcuts = remember { mutableStateOf(listOf<ScreenWithoutArguments>()) }
    val bottomShortcuts = remember { mutableStateOf(listOf<ScreenWithoutArguments>()) }
    val menuItems = remember { mutableStateOf(listOf<TopBarMenuItem>()) }
    val session = remember { prefs.sessionAsState }

    CompositionLocalProvider(
        LocalNavController provides navController,
        LocalSnackbarState provides snackbarState,
        LocalUseDarkTheme provides useDarkTheme,
        LocalTitle provides title,
        LocalDrawerAccess provides drawerAccess,
        LocalDrawerShortcuts provides drawerShortcuts,
        LocalBottomShortcuts provides bottomShortcuts,
        LocalMenuItems provides menuItems,
        LocalSession provides session,
    ) {
        AppFrameContent(
            scope = scope,
            navController = navController,
            drawerState = drawerState,
            topAppBarState = topAppBarState,
            snackbarState = snackbarState,
            drawerAccess = drawerAccess,
            drawerShortcuts = drawerShortcuts.value,
            bottomShortcuts = bottomShortcuts.value,
            session = session.value,
            currentRoute = null,
        ) { innerPadding ->
            // We replace the NavHost with a Box for the preview.
            Box(modifier = Modifier.padding(innerPadding)) {
                content()
            }
        }
    }
}

@Composable
fun <T: ScreenWithoutArguments> T.Preview() {
    PreparePreview { Show() }
}

@Composable
fun <T: ScreenWithArguments> T.Preview(arguments: Bundle) {
    PreparePreview { ShowPreview(arguments) }
}

fun <T> Flow<T?>.collectValue(): T? =
    runBlocking {
        var value: T? = null
        this@collectValue.collect { value = it }
        value
    }
