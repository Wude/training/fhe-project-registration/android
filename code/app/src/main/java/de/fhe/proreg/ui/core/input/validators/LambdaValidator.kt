package de.fhe.proreg.ui.core.input.validators

class LambdaValidator<T>(
    private val errorText: String,
    private val onValidate: () -> Boolean,
): Validator<T>() {
    override fun validate(value: T): String? =
        if (onValidate()) null else errorText
}
