package de.fhe.proreg.ui.core.input

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.TextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.FocusState
import androidx.compose.ui.focus.focusProperties
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import de.fhe.proreg.ui.core.input.validators.EmailValidator
import de.fhe.proreg.ui.core.input.validators.Validator

class EmailInput(
    label: String,
    value: String,
    onValueChange: (String) -> Unit,
    vararg validators: Validator<String>,
): TextInput(
    label = label,
    value = value,
    onValueChange = onValueChange,
    *validators,
) {
    @Composable
    fun show(
        modifier: Modifier = Modifier,
        placeholder: String? = null,
        enabled: Boolean = true,
        readOnly: Boolean = false,
        focusChanged: ((focus: FocusState) -> Unit)? = null,
        focusRequester: FocusRequester = FocusRequester(),
    ) = super.show(
            modifier = modifier,
            placeholder = placeholder,
            enabled = enabled,
            readOnly = readOnly,
            singleLine = true,
            maxLines = 1,
            focusChanged = focusChanged,
            focusRequester = focusRequester,
            imeAction = ImeAction.Next,
            keyboardType = KeyboardType.Email,
            keyboardActions = KeyboardActions.Default,
            leadingIcon = {
                Icon(
                    imageVector = Icons.Filled.Email,
                    contentDescription = label,
                )
            },
            trailingIcon = null,
        )
}
