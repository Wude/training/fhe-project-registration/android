package de.fhe.proreg.data

import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.data.network.model.StudentProjectDocument
import de.fhe.proreg.data.network.model.StudentProjectCreationRequest
//import de.fhe.proreg.domain.Repository
import kotlinx.coroutines.flow.Flow
import java.util.UUID

interface ProjectRepository {

    suspend fun downloadRelatedProjects(pageIndex: Int = 0, pageSize: Int = 20)

    suspend fun uploadProject(project: StudentProjectCreationRequest): UUID?

    fun getProjects(): Flow<List<StudentProject>>

    fun getProject(projectID: UUID?): Flow<StudentProject?>

    suspend fun addProject(project: StudentProject)

    fun getProjectDocument(projectDocumentID: UUID): Flow<StudentProjectDocument?>

    suspend fun addProjectDocument(projectDocument: StudentProjectDocument)
}
