package de.fhe.proreg.data.local

import androidx.room.Dao
import androidx.room.Database
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import de.fhe.proreg.data.converter.InstantConverter
import de.fhe.proreg.data.converter.UUIDConverter
import de.fhe.proreg.data.converter.BigDecimalConverter
import de.fhe.proreg.data.converter.ProjectStatusConverter
import de.fhe.proreg.data.converter.ProjectTypeConverter
import de.fhe.proreg.data.converter.UserTypeConverter
import de.fhe.proreg.data.local.model.StudentProjectEntity
import de.fhe.proreg.data.local.model.StudentProjectDocumentEntity
import de.fhe.proreg.data.local.model.UserEntity
import kotlinx.coroutines.flow.Flow
import java.util.UUID

@Database(entities = [UserEntity::class, StudentProjectEntity::class, StudentProjectDocumentEntity::class], version = 2, exportSchema = false)
@TypeConverters(
    UserTypeConverter::class, ProjectTypeConverter::class, ProjectStatusConverter::class,
    UUIDConverter::class, InstantConverter::class, BigDecimalConverter::class,
)
abstract class Db: RoomDatabase() {
    abstract fun userAccess(): UserAccess
    abstract fun studentProjectAccess(): StudentProjectAccess
    abstract fun studentProjectDocumentAccess(): StudentProjectDocumentAccess
}
