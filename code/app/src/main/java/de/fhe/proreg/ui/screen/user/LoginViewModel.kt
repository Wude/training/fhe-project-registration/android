package de.fhe.proreg.ui.screen.user

import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.Snapshot.Companion.withMutableSnapshot
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import de.fhe.proreg.usecase.LoginUseCase

@OptIn(SavedStateHandleSaveableApi::class)
class LoginViewModel(
    savedStateHandle: SavedStateHandle,
    private val loginUseCase: LoginUseCase,
): ViewModel() {
    var email by savedStateHandle.saveable { mutableStateOf("") }; private set
    var password by savedStateHandle.saveable { mutableStateOf("") }; private set

    fun updateEmail(value: String) {
        withMutableSnapshot {
            email = value
        }
    }

    fun updatePassword(value: String) {
        withMutableSnapshot {
            password = value
        }
    }

    suspend fun login() = loginUseCase(email, password)
}
