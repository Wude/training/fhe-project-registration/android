package de.fhe.proreg.data

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import de.fhe.proreg.data.network.model.Session
import de.fhe.proreg.data.network.model.Credentials
import de.fhe.proreg.data.network.model.UserType
import kotlinx.coroutines.flow.StateFlow
import java.util.UUID

interface PreferencesManager {
    var useDarkTheme: Boolean

    val userID: UUID?
    val userType: UserType?
    val isAdmin: Boolean
    val isFacultyBoardOfExaminers: Boolean

    val email: String?
    val password: String?
    val accessToken: String?

    var credentials: Credentials?
    var session: Session?

    val sessionAsState: State<Session?>
}
