package de.fhe.proreg.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.Instant
import java.util.UUID

@Entity
data class StudentProjectDocumentEntity(
    @PrimaryKey(autoGenerate = false) val projectDocumentID: UUID,
    val projectID: UUID,
    var title: String,
    var content: ByteArray,
    // @PrimaryKey(autoGenerate = false) val projectDocumentID: UUID = UUID.randomUUID(),
    var creationInstant: Instant = Instant.now(),
    var modificationInstant: Instant = Instant.now(),
)
