package de.fhe.proreg.data.converter

import android.webkit.MimeTypeMap
import de.fhe.proreg.util.Utils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.http.Part
import java.io.File
import java.lang.reflect.Type

fun getMimeType(file: File): String? {
    var type: String? = null
    val extension = MimeTypeMap.getFileExtensionFromUrl(file.extension)
    if (extension != null) {
        type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
    }
    return type
}

class StudentProjectDocumentConverterFactory : Converter.Factory() {

    override fun requestBodyConverter(
        type: Type,
        parameterAnnotations: Array<out Annotation>,
        methodAnnotations: Array<out Annotation>,
        retrofit: Retrofit,
    ): Converter<*, RequestBody>? {
        val isFileType = isFileType(type)

        if (!isFileType) {
            return null
        }

        val annotation = parameterAnnotations.find { annotation -> annotation is Part } as Part

        return Converter<Any, RequestBody> { value ->
            createMultipartBody(value as File, annotation)
        }
    }

    private fun createMultipartBody(
        file: File,
        annotation: Part,
    ): MultipartBody {
        val part = MultipartBody.Part.createFormData(annotation.value, file.name,
            file.asRequestBody(getMimeType(file)?.toMediaTypeOrNull())
        )
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addPart(part)

        return builder.build()
    }

    private fun isFileType(type: Type): Boolean = type == File::class.java
}
