package de.fhe.proreg.ui.core

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.navigation.NavHostController
import timber.log.Timber

abstract class ScreenWithoutArguments(
    icon: ImageVector,
    route: String,
    val displayRank: Int = Int.MAX_VALUE,
) : Screen(icon, route) {

    @Composable
    abstract fun Show()

    fun destination() = route
}
