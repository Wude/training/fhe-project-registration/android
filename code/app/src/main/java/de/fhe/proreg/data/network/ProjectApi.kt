package de.fhe.proreg.data.network

import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.data.network.model.StudentProjectCreationRequest
import de.fhe.proreg.data.network.model.StudentProjectCreationResponse
import de.fhe.proreg.data.network.model.StudentProjectDocument
import java.util.UUID
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Body

interface ProjectApi {
    @GET("project/year/{year}/paged/{pageIndex}/{pageSize}")
    @Headers("Accept: application/json")
    suspend fun getProjectsByYear(
        @Path("pageIndex") pageIndex: Int,
        @Path("pageSize") pageSize: Int,
        @Path("year") year: Int,
    ): Response<List<StudentProject>>

    @GET("project/related/paged/{pageIndex}/{pageSize}")
    @Headers("Accept: application/json")
    suspend fun getRelatedProjects(
        @Path("pageIndex") pageIndex: Int,
        @Path("pageSize") pageSize: Int,
    ): Response<List<StudentProject>>

    @GET("project/{projectId}")
    @Headers("Accept: application/json")
    suspend fun getProject(
        @Path("projectId") projectId: UUID,
    ): Response<StudentProject>

    @POST("project/create")
    @Headers(
        "Content-Type: application/json",
        "Accept: application/json",
    )
    suspend fun addProject(
        @Body project: StudentProjectCreationRequest,
    ): Response<StudentProjectCreationResponse>

    @POST("project/document/create")
    @Headers(
        "Content-Type: application/json",
        "Accept: application/json",
    )
    suspend fun addProjectDocument(
        @Body document: StudentProjectDocument,
    ): Response<Unit>

    @GET("project/document/{documentId}")
    @Headers("Accept: application/json")
    suspend fun getProjectDocument(
        @Path("documentId") documentId: UUID,
    ): Response<StudentProjectDocument>
}
