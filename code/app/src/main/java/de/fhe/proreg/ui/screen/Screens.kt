package de.fhe.proreg.ui.screen

import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.data.network.model.UserType
import de.fhe.proreg.ui.core.ScreenEnum
import de.fhe.proreg.ui.core.ScreenWithoutArguments
import de.fhe.proreg.ui.screen.boa.BoardOfExaminersScreen
import de.fhe.proreg.ui.screen.ea.ExaminationAuthorityScreen
import de.fhe.proreg.ui.screen.lecturer.LecturerScreen
import de.fhe.proreg.ui.screen.secretariat.SecretariatScreen
import de.fhe.proreg.ui.screen.student.StudentCreateProjectScreen
import de.fhe.proreg.ui.screen.student.StudentScreen
import de.fhe.proreg.ui.screen.student.StudentViewProjectScreen
import de.fhe.proreg.ui.screen.user.LoginScreen
import de.fhe.proreg.ui.screen.user.SettingsScreen
import de.fhe.proreg.ui.screen.user.StartScreen

object Screens: ScreenEnum() {
    override val Start = StartScreen

    val Login = LoginScreen
    val Student = StudentScreen
    val Lecturer = LecturerScreen
    val BoardOfExaminers = BoardOfExaminersScreen
    val Secretariat = SecretariatScreen
    val ExaminationAuthority = ExaminationAuthorityScreen
    val Settings = SettingsScreen

    val StudentCreateProject = StudentCreateProjectScreen
    val StudentViewProject = StudentViewProjectScreen

    override val withoutArguments = listOf(Start, Login, Settings,
        Student, StudentCreateProject,
        Lecturer, BoardOfExaminers, Secretariat, ExaminationAuthority)
    override val withArguments = listOf(StudentViewProject)

    override fun drawerShortcuts(prefs: PreferencesManager): List<ScreenWithoutArguments> {
        val shortcuts = mutableListOf<ScreenWithoutArguments>()
        if (prefs.userType != null) {
            if (prefs.userType!! == UserType.STUDENT) {
                shortcuts.add(Student)
            }
            else if (prefs.userType!! == UserType.LECTURER) {
                shortcuts.add(Lecturer)
            }
            else if (prefs.userType!! == UserType.FACULTY_SECRETARIAT_STAFF) {
                shortcuts.add(Secretariat)
            }
            else if (prefs.userType!! == UserType.EXAMINATION_AUTHORITY_STAFF) {
                shortcuts.add(ExaminationAuthority)
            }
            // if (prefs.userType!! == UserType.ADMINISTRATOR_STAFF || prefs.isAdmin) {
            //     shortcuts.add(Admin)
            // }

            if (prefs.isFacultyBoardOfExaminers) {
                shortcuts.add(BoardOfExaminers)
            }
        }
        shortcuts.add(Settings)
        return shortcuts
    }

    override fun bottomShortcuts(prefs: PreferencesManager): List<ScreenWithoutArguments> {
        val shortcuts = mutableListOf<ScreenWithoutArguments>()
        if (prefs.userType != null) {
            if (prefs.isFacultyBoardOfExaminers) {
                if (prefs.userType!! == UserType.LECTURER) {
                    shortcuts.add(Lecturer)
                }
                shortcuts.add(BoardOfExaminers)
            }
        }
        return shortcuts
    }
}
