@file:OptIn(ExperimentalMaterial3Api::class)

package de.fhe.proreg.ui.core

import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState

@Composable
fun BottomBar(
    navController: NavHostController,
    currentRoute: String?,
    bottomShortcuts: List<ScreenWithoutArguments>
) {
    if (bottomShortcuts.any()) {
        NavigationBar {
            bottomShortcuts.forEach { screen ->
                NavigationBarItem(
                    icon = { Icon(screen.icon, screen.title) },
                    label = { Text(screen.title) },
                    selected = currentRoute == screen.route,
                    onClick = { navController.navigateUpTo(screen.destination()) }
                )
            }
        }
    }
}
