package de.fhe.proreg.usecase

import de.fhe.proreg.data.ProjectRepository
import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.data.network.model.StudentProjectDocument
import kotlinx.coroutines.flow.Flow
import java.util.UUID

class GetProjectUseCaseProper(
    private val projectRepository: ProjectRepository,
) : GetProjectUseCase {
    override operator fun invoke(projectID: UUID): Flow<StudentProject?> {
        return projectRepository.getProject(projectID)
    }
}

class GetProjectDocumentUseCaseProper(
    private val projectRepository: ProjectRepository,
) : GetProjectDocumentUseCase {
    override operator fun invoke(projectDocumentID: UUID): Flow<StudentProjectDocument?> {
        return projectRepository.getProjectDocument(projectDocumentID)
    }
}
