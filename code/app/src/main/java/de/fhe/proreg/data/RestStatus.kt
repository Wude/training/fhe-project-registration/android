package de.fhe.proreg.data

object RestStatusInt {
    const val OK_200 = 200
    const val CREATED_201 = 201
    const val NO_CONTENT_204 = 204
    const val BAD_REQUEST_400 = 400
    const val UNAUTHORIZED_401 = 401
    const val REQUEST_TIMEOUT_408 = 408
    const val INTERNAL_SERVER_ERROR_500 = 500
}

object RestStatusString {
    const val OK_200 = "200 OK"
    const val CREATED_201 = "201 Created"
    const val NO_CONTENT_204 = "204 No Content"
    const val BAD_REQUEST_400 = "400 Bad Request"
    const val UNAUTHORIZED_401 = "401 Unauthorized"
    const val REQUEST_TIMEOUT_408 = "408 Request Timeout"
    const val INTERNAL_SERVER_ERROR_500 = "500 Internal Server Error"
}
