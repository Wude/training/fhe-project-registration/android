package de.fhe.proreg.util

import java.util.concurrent.locks.ReentrantReadWriteLock

inline fun <R> ReentrantReadWriteLock.read(crossinline action: () -> R): R {
    this.readLock().lock()
    try { return action() }
    finally { this.readLock().unlock() }
}

inline fun <R> ReentrantReadWriteLock.write(crossinline action: () -> R): R {
    this.writeLock().lock()
    try { return action() }
    finally { this.writeLock().unlock() }
}
