package de.fhe.proreg.data.network.model

import java.time.Instant
import java.util.UUID

data class StudentProjectDocument(
    val projectID: UUID,
    var title: String,
    var content: ByteArray,
    val projectDocumentID: UUID = UUID.randomUUID(),
    var creationInstant: Instant = Instant.now(),
    var modificationInstant: Instant = Instant.now(),
) {
    constructor(
        projectID: UUID,
        title: String,
        content: ByteArray
    ): this(
        projectID,
        title,
        content,
        projectDocumentID = UUID.randomUUID(),
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as StudentProjectDocument

        if (projectID != other.projectID) return false
        if (title != other.title) return false
        if (!content.contentEquals(other.content)) return false
        if (projectDocumentID != other.projectDocumentID) return false
        if (creationInstant != other.creationInstant) return false
        if (modificationInstant != other.modificationInstant) return false

        return true
    }

    override fun hashCode(): Int {
        var result = projectID.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + content.contentHashCode()
        result = 31 * result + projectDocumentID.hashCode()
        result = 31 * result + creationInstant.hashCode()
        result = 31 * result + modificationInstant.hashCode()
        return result
    }
}
