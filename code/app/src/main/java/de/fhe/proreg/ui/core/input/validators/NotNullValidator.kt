package de.fhe.proreg.ui.core.input.validators

class NotNullValidator<T: Any>(
    private val errorText: String = "Auswahl erforderlich",
): Validator<T?>() {
    override fun validate(value: T?): String? =
        if (value == null) errorText else null
}
