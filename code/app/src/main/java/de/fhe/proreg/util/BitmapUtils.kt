package de.fhe.proreg.util

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Base64
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.nio.ByteBuffer
import kotlin.math.min

/**
 * Converts a byte array to a bitmap
 *
 * @return The resulting bitmap.
 */
fun ByteArray.toBitmap(): Bitmap =
    BitmapFactory.decodeByteArray(this, 0, size)

/**
 * Converts bitmap to byte array givable a compression format.
 *
 * @param format  The compression format to apply.
 * @param quality The compression quality to apply.
 * @return The resulting byte array.
 */
fun Bitmap.toByteArray(
    format: Bitmap.CompressFormat = Bitmap.CompressFormat.PNG,
    quality: Int = 0,
): ByteArray {
    var stream: ByteArrayOutputStream? = null
    return try {
        stream = ByteArrayOutputStream()
        compress(format, quality, stream)
        stream.toByteArray()
    } finally {
        if (stream != null) {
            try {
                stream.close()
            } catch (e: IOException) {
                Timber.e("ByteArrayOutputStream was not closed")
            }
        }
    }
}

/**
 * Converts bitmap to byte array without compression.
 *
 * @return The resulting byte array.
 */
fun Bitmap.toByteArrayUncompressed(): ByteArray = ByteBuffer.allocate(byteCount).apply {
        copyPixelsToBuffer(this)
        rewind()
    }.array()

/**
 * Scales a bitmap to fit in the givable maximum dimension.
 *
 * @param  maxWidthOrHeight The maximum width or height for the bitmap.
 * @return The resulting bitmap.
 */
fun Bitmap.scale(maxWidthOrHeight: Int): Bitmap {
    val factor = min(
        maxWidthOrHeight.toDouble() / width.toDouble(),
        maxWidthOrHeight.toDouble() / height.toDouble()).toFloat()
    return Bitmap.createScaledBitmap(
            this,
            (width.toFloat() * factor).toInt(),
            (height.toFloat() * factor).toInt(),
            true)
}

/**
 * Scales a bitmap to fit in the givable maximum dimension.
 *
 * @param  maxWidth  The maximum width for the bitmap.
 * @param  maxHeight The maximum height for the bitmap.
 * @return The resulting bitmap.
 */
fun Bitmap.scale(maxWidth: Int, maxHeight: Int): Bitmap {
    val factor = min(
        maxWidth.toDouble() / width.toDouble(),
        maxHeight.toDouble() / height.toDouble()).toFloat()
    return Bitmap.createScaledBitmap(
            this,
            (width.toFloat() * factor).toInt(),
            (height.toFloat() * factor).toInt(),
            true)
}

/**
 * Centers a bitmap and crops the sides of the greater dimension.
 *
 * @return The resulting bitmap.
 */
fun Bitmap.centerCrop(): Bitmap = when {
        width >= height -> {
            Bitmap.createBitmap(this, (width / 2) - (height / 2), 0, height, height)
        }
        width <= height -> {
            Bitmap.createBitmap(this, 0, (height / 2) - (width / 2), width, width)
        }
        else -> {
            this
        }
    }

@SuppressLint("ObsoleteSdkInt")
fun String?.decodeBase64(): Bitmap? =
    if (this == null) {
        null
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        java.util.Base64.getDecoder().decode(this).toBitmap()
    } else {
        Base64.decode(this, Base64.DEFAULT).toBitmap()
    }

@SuppressLint("ObsoleteSdkInt")
fun Bitmap?.encodeBase64(): String? =
    if (this == null) {
        null
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        java.util.Base64.getEncoder().encodeToString(this.toByteArray())
    } else {
        Base64.encodeToString(this.toByteArray(), Base64.DEFAULT)
    }
