package de.fhe.proreg.data.network.model

/**
* The status of a project.
*/
enum class ProjectStatus(val code: String) {
    /**
    * The project was merely submitted.
    */
    SUBMITTED("SUB"),

    /**
    * The project was rejected.
    */
    REJECTED("REJ"),

    /**
    * The project was accepted.
    */
    ACCEPTED("ACC"),

    /**
    * The project was registered.
    */
    REGISTERED("REG"),

    /**
    * The a deadline extension for the project has been requested.
    */
    DEADLINE_EXTENSION_REQUESTED("DEQ"),

    /**
    * The a requested deadline extension for the project has been rejected.
    */
    DEADLINE_EXTENSION_REJECTED("DEJ"),

    /**
    * The a requested deadline extension for the project has been granted.
    */
    DEADLINE_EXTENSION_GRANTED("DEG"),

    /**
    * The project has been graded.
    */
    GRADED("GRA"),

    /**
    * The project has been completed (either passed or failed).
    */
    COMPLETED("COM");

    companion object {

        val valuesByCode = HashMap<String, ProjectStatus>()

        init {
            val projectStatusValues = ProjectStatus.values()

            for (projectStatus in projectStatusValues) {
                valuesByCode.put(projectStatus.code, projectStatus);
                valuesByCode.put(projectStatus.name, projectStatus);
            }
        }

        fun tryValueOf(idOrCode: String?, defaultValue: ProjectStatus): ProjectStatus = valuesByCode.get(idOrCode) ?: defaultValue
        fun tryValueOf(idOrCode: String?): ProjectStatus? = valuesByCode.get(idOrCode)
    }
}
