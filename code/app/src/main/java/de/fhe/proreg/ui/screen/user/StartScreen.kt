package de.fhe.proreg.ui.screen.user

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Work
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.tooling.preview.Preview
import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.data.network.model.UserType
import de.fhe.proreg.ui.core.DrawerAccessState
import de.fhe.proreg.ui.core.LocalNavController
import de.fhe.proreg.ui.core.PrepareFrame
import de.fhe.proreg.ui.core.Preview
import de.fhe.proreg.ui.core.ScreenWithoutArguments
import de.fhe.proreg.ui.core.navigateUpTo
import de.fhe.proreg.ui.screen.Screens
import kotlinx.coroutines.launch
import org.koin.compose.koinInject

object StartScreen: ScreenWithoutArguments(
    icon = Icons.Filled.Work,
    route = "start",
) {
    override val title = "FHE-Projektregistrierung"

    @Composable
    override fun Show() {
        PrepareFrame(
            StartScreen.title,
            DrawerAccessState.HIDDEN,
        )

        val scope = rememberCoroutineScope()

        // Redirect to the proper start pages
        val prefs = koinInject<PreferencesManager>()
        val navController = LocalNavController.current
        LaunchedEffect(prefs, navController) {
            scope.launch {
                if (prefs.accessToken == null && prefs.userType == null) {
                    navController.navigateUpTo(Screens.Login.destination(), saveState = false)
                }
                else {
                    if (prefs.userType == UserType.STUDENT) {
                        navController.navigateUpTo(Screens.Student.destination(), saveState = false)
                    }
                    else if (prefs.userType == UserType.LECTURER) {
                        navController.navigateUpTo(Screens.Lecturer.destination(), saveState = false)
                    }
                    else if (prefs.userType == UserType.FACULTY_SECRETARIAT_STAFF) {
                        navController.navigateUpTo(Screens.Secretariat.destination(), saveState = false)
                    }
                    else if (prefs.userType == UserType.EXAMINATION_AUTHORITY_STAFF) {
                        navController.navigateUpTo(Screens.ExaminationAuthority.destination(), saveState = false)
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewStart() {
    StartScreen.Preview()
}
