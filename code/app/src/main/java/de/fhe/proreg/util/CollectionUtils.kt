package de.fhe.proreg.util

/**
 * Returns a set containing all elements from both collections.
 * 
 * The returned set preserves the element iteration order of the original collection.
 * Those elements of the [other] collection that are unique are iterated in the end
 * in the order of the [other] collection.
 * 
 * To get a set containing all elements that are contained in both collections use [intersect].
 */
infix fun <T> Iterable<T>.unionAll(other: Iterable<T>): List<T> {
    val list = this.toMutableList()
    list.addAll(other)
    return list
}
