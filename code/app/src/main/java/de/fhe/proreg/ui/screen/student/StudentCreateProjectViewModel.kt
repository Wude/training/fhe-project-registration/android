package de.fhe.proreg.ui.screen.student

import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.Snapshot.Companion.withMutableSnapshot
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asAndroidBitmap
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.SavedStateHandleSaveableApi
import androidx.lifecycle.viewmodel.compose.saveable
import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.data.network.model.ProjectType
import de.fhe.proreg.data.network.model.User
import de.fhe.proreg.ui.core.saveableNullableState
import de.fhe.proreg.util.encodeBase64
import de.fhe.proreg.usecase.CreateProjectUseCase
import de.fhe.proreg.usecase.DownloadLecturersUseCase
import de.fhe.proreg.usecase.GetLecturersUseCase
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.UUID

@OptIn(SavedStateHandleSaveableApi::class)
class StudentCreateProjectViewModel(
    savedStateHandle: SavedStateHandle,
    private val prefs: PreferencesManager,
    private val getLecturersUseCase: GetLecturersUseCase,
    private val downloadLecturersUseCase: DownloadLecturersUseCase,
    private val createProjectUseCase: CreateProjectUseCase,
): ViewModel() {

    init {
        downloadLecturers()
    }

    val lecturers: StateFlow<List<User>> = getLecturersUseCase().stateIn(
            initialValue = listOf(),
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000)
        )

    // var projectID = savedStateHandle.saveableNullableState("projectID") { mutableStateOf<UUID?>(null) }; private set
    var projectType by savedStateHandle.saveable { mutableStateOf(ProjectType.BACHELORS_PROJECT) }; private set
    var title by savedStateHandle.saveable { mutableStateOf("") }; private set
    var supervisingTutor1 = savedStateHandle.saveableNullableState("supervisingTutor1") { mutableStateOf<User?>(null) }; private set
    var supervisingTutor2 = savedStateHandle.saveableNullableState("supervisingTutor2") { mutableStateOf<User?>(null) }; private set
    var signatureBitmap = savedStateHandle.saveableNullableState("signatureBitmap") { mutableStateOf<ImageBitmap?>(null) }; private set

    // fun updateProjectID(value: UUID) { withMutableSnapshot { this.projectID.value = value } }
    fun updateProjectType(value: ProjectType?) { withMutableSnapshot {
        if (value != null) {
            this.projectType = value
            if (value == ProjectType.BACHELORS_PROJECT || value == ProjectType.MASTERS_PROJECT) {
                this.supervisingTutor2.value = null
            }
        }
    } }
    fun updateTitle(value: String) { withMutableSnapshot { this.title = value } }
    fun updateSupervisingTutor1(value: User?) { withMutableSnapshot { this.supervisingTutor1.value = value } }
    fun updateSupervisingTutor2(value: User?) { withMutableSnapshot { this.supervisingTutor2.value = value } }
    fun updateSignatureBitmap(value: ImageBitmap?) { withMutableSnapshot { this.signatureBitmap.value = value } }

    fun getLecturers() = getLecturersUseCase()
    private fun downloadLecturers() { runBlocking { downloadLecturersUseCase() } }
    suspend fun createProject(): Boolean {
        val userID = prefs.userID
        if (userID != null && supervisingTutor1.value != null && signatureBitmap.value != null) {
            val projectID = createProjectUseCase(
                projectType = projectType,
                studentID = userID,
                studentEmail = prefs.email!!,
                supervisingTutor1ID = supervisingTutor1.value!!.userID,
                supervisingTutor1Email = supervisingTutor1.value!!.email,
                supervisingTutor2ID = supervisingTutor2.value?.userID,
                supervisingTutor2Email = supervisingTutor2.value!!.email,
                title = title,
                studentSignature = signatureBitmap.value!!.asAndroidBitmap().encodeBase64()!!,
            )
            return projectID != null
        }
        return false
    }
}
