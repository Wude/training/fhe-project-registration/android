package de.fhe.proreg.util

import kotlin.reflect.KCallable
import kotlin.reflect.KClass

val <R> KCallable<R>.returnClass: KClass<*>
    get() = this.returnType.classifier as KClass<*>
