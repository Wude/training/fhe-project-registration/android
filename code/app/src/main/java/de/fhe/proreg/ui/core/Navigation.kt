@file:OptIn(ExperimentalMaterial3Api::class)

package de.fhe.proreg.ui.core

import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable

@Composable
fun Navigation(
    screens: ScreenEnum,
    navController: NavHostController,
    startDestination: String,
    modifier: Modifier = Modifier,
) {
    NavHost(
        navController = navController,
        startDestination = startDestination,
        modifier = modifier,
    ) {
        screens.all.forEach { screen ->
            composable(screen.route, screen.arguments) { entry ->
                when (screen) {
                    is ScreenWithoutArguments -> screen.Show()
                    is ScreenWithArguments -> screen.Show(entry.arguments!!)
                }
            }
        }
    }
}
