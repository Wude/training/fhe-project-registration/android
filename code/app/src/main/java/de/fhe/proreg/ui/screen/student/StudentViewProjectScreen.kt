package de.fhe.proreg.ui.screen.student

import android.os.Bundle
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavType
import androidx.navigation.navArgument
import de.fhe.proreg.data.ProjectRepository
import de.fhe.proreg.data.network.model.ProjectStatus
import de.fhe.proreg.data.network.model.ProjectType
import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.ui.core.DrawerAccessState
import de.fhe.proreg.ui.core.FloatingActionButton
import de.fhe.proreg.ui.core.LocalBottomShortcuts
import de.fhe.proreg.ui.core.PrepareFrame
import de.fhe.proreg.ui.core.Preview
import de.fhe.proreg.ui.core.ScreenWithArguments
import de.fhe.proreg.ui.core.TextDisplay
import de.fhe.proreg.ui.core.TopBarMenuItem
import de.fhe.proreg.ui.core.collectValue
import de.fhe.proreg.ui.screen.Screens
import de.fhe.proreg.ui.screen.projectStatusDescription
import de.fhe.proreg.util.TimeUtils
import de.fhe.proreg.util.toGermanDateTime
import de.fhe.proreg.util.toLongText
import de.fhe.proreg.util.toMediumText
import kotlinx.coroutines.runBlocking
import org.koin.androidx.compose.koinViewModel
import org.koin.compose.koinInject
import java.util.UUID

object StudentViewProjectScreen: ScreenWithArguments(
    icon = Icons.Filled.Person,
    route = "student/project/view",
    navArgument("projectId") { type = NavType.StringType },
) {
    override val title = "Projektansicht"

    @Composable
    override fun Show(arguments: Bundle) {
        val projectID = UUID.fromString(arguments.getString("projectId"))

        val vm: StudentViewProjectViewModel = koinViewModel()
        val project by remember(vm) { vm.getProject(projectID) }.collectAsStateWithLifecycle(null)

        PrepareFrame(
            project?.title ?: title,
            DrawerAccessState.BACK,
        )
        if (project != null) {
            ShowProject(project!!)
        }
    }

    @Composable
    override fun ShowPreview(arguments: Bundle) {
        val projectID = UUID.fromString(arguments.getString("projectId"))
        val projectRepository = koinInject<ProjectRepository>()
        val project = projectRepository.getProject(projectID).collectValue()

        PrepareFrame(
            project?.title ?: title,
            DrawerAccessState.BACK,
        )
        if (project != null) {
            ShowProject(project)
        }
    }

    fun destination(projectId: String) = destinationWithArguments(projectId)
}

@Composable
fun ShowProject(project: StudentProject) {
    Box(modifier = Modifier.fillMaxSize()) {
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .padding(12.dp)
                .fillMaxSize(),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            TextDisplay(
                value = project.title,
                label = "Titel",
            )

            TextDisplay(
                value = projectStatusDescription(project.status),
                label = "Status",
            )

            TextDisplay(
                value = project.supervisingTutor1Email,
                label = when(project.projectType) {
                    ProjectType.BACHELORS_THESIS -> "Erstgutachter"
                    ProjectType.MASTERS_THESIS -> "Erstgutachter"
                    else -> "Gutachter"
                },
            )

            if (project.projectType == ProjectType.BACHELORS_THESIS ||
                project.projectType == ProjectType.MASTERS_THESIS
            ) {
                TextDisplay(
                    value = "${project.supervisingTutor2Email}",
                    label = "Zweitgutachter",
                )

                if (project.deadline != null) {
                    TextDisplay(
                        value = project.deadline!!.toGermanDateTime().toMediumText(),
                        label = "Frist",
                    )
                }
            }

            TextDisplay(
                value = project.creationInstant.toGermanDateTime().toMediumText(),
                label = "Erstellungszeitpunkt",
            )

            if (project.grade != null) {
                TextDisplay(
                    value = "${project.grade}",
                    label = "Note",
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewViewProject() {
    val arguments = Bundle()
    arguments.putString("projectId", "6510aaf1-9462-62a7-a770-775746567316")
    StudentViewProjectScreen.Preview(arguments)
}
