package de.fhe.proreg.data

import de.fhe.proreg.data.network.model.StudentProject
import de.fhe.proreg.data.network.model.StudentProjectCreationRequest
import de.fhe.proreg.data.network.model.StudentProjectDocument
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.util.UUID

class ProjectRepositoryFake : ProjectRepository {
    private val projectsById = mutableMapOf<UUID, StudentProject>()
    private val projectDocumentsById = mutableMapOf<UUID, StudentProjectDocument>()

    override suspend fun downloadRelatedProjects(pageIndex: Int, pageSize: Int) { }

    override suspend fun uploadProject(project: StudentProjectCreationRequest): UUID? { return project.projectID }

    override fun getProjects(): Flow<List<StudentProject>> = flow {
        emit(projectsById.values.toList())
    }

    override fun getProject(projectID: UUID?): Flow<StudentProject?> = flow {
        emit(if (projectID == null) null else projectsById[projectID])
    }

    override suspend fun addProject(project: StudentProject) {
        projectsById[project.projectID] = project
    }

    override fun getProjectDocument(projectDocumentID: UUID): Flow<StudentProjectDocument?> = flow {
        emit(projectDocumentsById[projectDocumentID])
    }

    override suspend fun addProjectDocument(projectDocument: StudentProjectDocument) {
        projectDocumentsById[projectDocument.projectDocumentID] = projectDocument
    }
}
