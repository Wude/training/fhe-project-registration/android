@file:Suppress("SpellCheckingInspection", "unused", "MemberVisibilityCanBePrivate")

package de.fhe.proreg.data.adapter

import de.fhe.proreg.util.format
import de.fhe.proreg.util.parseBigDecimal
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.math.BigDecimal

class BigDecimalJsonAdapter {

    @FromJson
    fun fromJson(json: String): BigDecimal = json.parseBigDecimal()!!

    @ToJson
    fun toJson(value: BigDecimal): String = value.format()!!

}
