package de.fhe.proreg.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import de.fhe.proreg.data.network.model.UserType
import java.util.UUID

@Entity
data class UserEntity(
    @PrimaryKey(autoGenerate = false) val userID: UUID = UUID.randomUUID(),
    val email: String,
    var userType: UserType,
    var admin: Boolean,
    var facultyBoardOfExaminers: Boolean,
    // var faculties: List<Faculty>
    // var departments: List<Department>
)
