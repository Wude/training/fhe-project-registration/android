package de.fhe.proreg.data.network.model

import java.util.UUID

data class Session(
    var accessToken: String,
    var user: User)
