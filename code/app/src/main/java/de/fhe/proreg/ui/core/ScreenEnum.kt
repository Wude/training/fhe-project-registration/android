package de.fhe.proreg.ui.core

import de.fhe.proreg.data.PreferencesManager
import de.fhe.proreg.util.returnClass
import de.fhe.proreg.util.unionAll
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.isSubclassOf

abstract class ScreenEnum {
    abstract val Start: ScreenWithoutArguments

    abstract val withoutArguments: List<ScreenWithoutArguments>
    abstract val withArguments: List<ScreenWithArguments>

    val all by lazy { withoutArguments unionAll withArguments }
    val allByRoute by lazy { all.associateBy { it.route } }
    val withoutArgumentsByRoute by lazy { withoutArguments.associateBy { it.route } }
    val withArgumentsByRoute by lazy { withArguments.associateBy { it.route } }

    abstract fun drawerShortcuts(prefs: PreferencesManager): List<ScreenWithoutArguments>

    abstract fun bottomShortcuts(prefs: PreferencesManager): List<ScreenWithoutArguments>
}
