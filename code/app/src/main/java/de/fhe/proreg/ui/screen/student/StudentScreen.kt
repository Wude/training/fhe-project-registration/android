package de.fhe.proreg.ui.screen.student

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import de.fhe.proreg.ui.core.DrawerAccessState
import de.fhe.proreg.ui.core.FloatingActionButton
import de.fhe.proreg.ui.core.LocalDrawerAccess
import de.fhe.proreg.ui.core.LocalMenuItems
import de.fhe.proreg.ui.core.PrepareFrame
import de.fhe.proreg.ui.core.Preview
import de.fhe.proreg.ui.core.ScreenWithoutArguments
import de.fhe.proreg.ui.core.TopBarMenuItem
import de.fhe.proreg.ui.core.LocalNavController
import de.fhe.proreg.ui.core.LocalTitle
import de.fhe.proreg.ui.core.navigateTo
import de.fhe.proreg.ui.screen.Screens
import de.fhe.proreg.ui.screen.user.ProjectListCard
import kotlinx.coroutines.launch
import org.koin.androidx.compose.koinViewModel

object StudentScreen: ScreenWithoutArguments(
    icon = Icons.Filled.Person,
    route = "student/project/list",
    displayRank = 1,
) {
    override val title = "Projekte"

    @Composable
    override fun Show() {
        val scope = rememberCoroutineScope()
        val navController = LocalNavController.current
        val onAdd: () -> Unit = {
            scope.launch {
                navController.navigateTo(Screens.StudentCreateProject.destination())
            }
        }

        PrepareFrame(
            StudentScreen.title,
            DrawerAccessState.ACCESSIBLE,
            TopBarMenuItem(
                icon = Icons.Rounded.Add,
                title = "Erstellen",
                action = onAdd,
            ),
        )

        val vm: StudentViewModel = koinViewModel()
        val projects by remember(vm) { vm.projects }.collectAsStateWithLifecycle(listOf())

        Box(modifier = Modifier.fillMaxSize()) {

            FloatingActionButton(Icons.Rounded.Add, "Erstellen", onClick = onAdd)

            val lazyListState = rememberLazyListState()
            LazyColumn(state = lazyListState) {
                items(
                    items = projects,
                    key = { it.projectID },
                ) {
                    ProjectListCard(
                        project = it,
                        onClick = {
                            scope.launch {
                                navController.navigateTo(
                                    Screens.StudentViewProject.destination(it.projectID.toString()))
                            }
                        })
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun Preview() {
    StudentScreen.Preview()
}
