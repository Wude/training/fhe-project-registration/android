package de.fhe.proreg.ui.core.input

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusProperties
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import de.fhe.proreg.ui.core.input.validators.LambdaValidator
import de.fhe.proreg.ui.core.input.validators.NotNullValidator
import de.fhe.proreg.ui.core.input.validators.Validator
import de.fhe.proreg.util.unionAll

class SingleSelectInput<T: Any>(
    label: String,
    value: T?,
    onValueChange: (value: T?) -> Unit,
    val onDisplayValue: (value: T) -> String,
    val values: State<List<T>>,
    val onSearch: (query: String) -> Unit,
    vararg validators: Validator<T?>?,
): Input<T?>(
    label = label,
    value = value,
    onValueChange = onValueChange,
    validators = validators,
) {
    private fun filter(values: List<T>, query: String) =
        values.filter { value ->
            onDisplayValue(value).lowercase().contains(query.lowercase())
        }

    @Composable
    fun show(
        modifier: Modifier = Modifier,
        enabled: Boolean = true,
        searchable: Boolean = true,
        nonNullValue: Boolean = true,
        leadingIcon: @Composable (() -> Unit)? = null,
    ): SingleSelectInput<T> {
        errorTextLines = remember { mutableStateOf(listOf()) }

        val minIndex = if (nonNullValue) 0 else -1
        var visible by remember { mutableStateOf(false) }
        val query = remember { mutableStateOf("") }
        val values by remember { values }
        val valuesFiltered by remember { derivedStateOf { filter(values, query.value) } }
        var selectedIndex by remember { mutableStateOf(minIndex) }
        val focusRequester = FocusRequester()

        Column(modifier = modifier) {
            TextField(
                value = if (value != null) {
                    onDisplayValue(value)
                } else {
                    ""
                },
                onValueChange = { },
                modifier = Modifier
                    .fillMaxWidth()
                    .focusProperties { right }
                    .focusRequester(focusRequester)
                    .onFocusChanged { visible = it.isFocused },
                enabled = enabled,
                readOnly = true,
                label = { Text(label) },
                leadingIcon = leadingIcon,
                trailingIcon = {
                    Icon(
                        if (visible) {
                            Icons.Filled.KeyboardArrowUp
                        } else {
                            Icons.Filled.KeyboardArrowDown
                        },
                        null
                    )
                },
                isError = hasError,
                visualTransformation = VisualTransformation.None,
                keyboardOptions = KeyboardOptions(
                    capitalization = KeyboardCapitalization.None,
                    autoCorrect = false,
                    imeAction = ImeAction.Next,
                    keyboardType = KeyboardType.Text,
                ),
                keyboardActions = KeyboardActions(),
                singleLine = true,
                colors = OutlinedTextFieldDefaults.colors(),
            )

            if (hasError) {
                Text(
                    text = errorTextLines.value.joinToString("\n"),
                    modifier = Modifier.padding(
                        horizontal = 8.dp,
                        vertical = 4.dp
                    ),
                    color = MaterialTheme.colorScheme.error,
                )
            }
        }

        if (visible) {
            val focusManager = LocalFocusManager.current
            val heightInDp = LocalConfiguration.current.screenHeightDp.dp

            val onDismissRequest: () -> Unit = {
                visible = false
                focusManager.clearFocus()
            }

            Dialog(onDismissRequest = onDismissRequest) {
                Surface(
                    modifier = Modifier.width(300.dp),
                    shape = RoundedCornerShape(10.dp),
                ) {
                    Column {
                        Column(
                            modifier = Modifier.padding(16.dp)
                        ) {
                            Text(text = label, style = MaterialTheme.typography.headlineSmall)

                            Spacer(modifier = Modifier.height(16.dp))

                            if (searchable) {
                                val keyboardAction = {
                                    onSearch(query.value)
                                    //valuesFiltered = filter(values, query.value)
                                    selectedIndex = minIndex
                                }
                                TextInput(
                                    label = stringResource(id = android.R.string.search_go),
                                    value = query.value,
                                    onValueChange = {
                                        query.value = it
                                        //valuesFiltered = filter(values, query.value)
                                        selectedIndex = minIndex
                                    },
                                ).show(
                                    singleLine = true,
                                    leadingIcon = {
                                        IconButton(onClick = { keyboardAction() }) {
                                            Icon(
                                                Icons.Filled.Search,
                                                stringResource(id = android.R.string.search_go),
                                            )
                                        }
                                    },
                                    trailingIcon = {
                                        IconButton(onClick = {
                                            query.value = ""
                                            //valuesFiltered = values
                                            selectedIndex = minIndex
                                        }) {
                                            Icon(Icons.Filled.Clear, null)
                                        }
                                    },
                                    imeAction = ImeAction.Search,
                                    keyboardActions = KeyboardActions(
                                        onDone = { keyboardAction() },
                                        onSend = { keyboardAction() },
                                        onSearch = { keyboardAction() },
                                    ),
                                )
                            }
                        }

                        LazyColumn(
                            modifier = Modifier.height((heightInDp / 5) * 3)
                        ) {
                            items(
                                items = (minIndex until valuesFiltered.size).toList(),
                                key = { it },
                            ) { it ->
                                val selected = it == selectedIndex

                                Row(
                                    Modifier
                                        .fillMaxWidth()
                                        .selectable(
                                            selected = it == selectedIndex,
                                            onClick = { selectedIndex = it },
                                        ).background(
                                            if (selected) {
                                                MaterialTheme.colorScheme.onPrimaryContainer
                                            } else {
                                                MaterialTheme.colorScheme.surface
                                            }
                                        )
                                ) {
                                    Text(
                                        text = if (it >= 0) {
                                                onDisplayValue(valuesFiltered[it])
                                            } else {
                                                "-"
                                            },
                                        style = MaterialTheme.typography.bodyLarge.merge(),
                                        color = if (selected) {
                                                MaterialTheme.colorScheme.onPrimary
                                            } else {
                                                MaterialTheme.colorScheme.onSurface
                                            },
                                        modifier = Modifier
                                            .padding(8.dp)
                                            .align(alignment = Alignment.CenterVertically),
                                    )
                                }
                            }
                        }

                        Column(modifier = Modifier.padding(16.dp)) {
                            Spacer(modifier = Modifier.height(16.dp))

                            Row(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalArrangement = Arrangement.End
                            ) {
                                TextButton(
                                    onClick = onDismissRequest,
                                    shape = MaterialTheme.shapes.medium
                                ) {
                                    Text(text = stringResource(id = android.R.string.cancel))
                                }
                                Spacer(
                                    modifier = Modifier.width(16.dp)
                                )

                                Button(
                                    onClick = {
                                        if (selectedIndex < 0) {
                                            onValueChange(null)
                                            validate(null)
                                        } else if (
                                            (selectedIndex >= 0) &&
                                            (selectedIndex < valuesFiltered.size)
                                        ) {
                                            onValueChange(valuesFiltered[selectedIndex])
                                            validate(valuesFiltered[selectedIndex])
                                        }
                                        onDismissRequest()
                                    },
                                    shape = MaterialTheme.shapes.medium,
                                ) {
                                    Text(text = stringResource(id = android.R.string.ok))
                                }
                            }
                        }
                    }
                }
            }
        }
        return this
    }
}
